=======
##2.0.1

Use 2.0.1.1 for bitcode disabled
Use 2.0.1.2 for bitcode enabled

- Fixed an issue with nil session configuration object.

=======
##2.0.0

Use 2.0.0.1 for bitcode disabled
Use 2.0.0.2 for bitcode enabled

- Performance improvements and bug fixes.

=======
##1.2.1

Use 1.2.1.1 for bitcode disabled
Use 1.2.1.2 for bitcode enabled

- Fixed an issue with request redirects.
- Fixed other minor bugs.

=======
##1.2.0

Use 1.2.0.1 for bitcode disabled
Use 1.2.0.2 for bitcode enabled

- GA release.
