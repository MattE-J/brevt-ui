# Brevt - Discover Quality Information

## Overview
The iOS frontend application of the Brevt application.

## Resources: 
__Website__ - https://brevt.com/  
__App Store__ - https://itunes.apple.com/us/app/brevt/id1363796061?mt=8

## Contributors
- Sam Jensen
- Matt Jackels