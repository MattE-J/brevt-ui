//
//  ArticleCollectionViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage
import Alamofire
import Firebase
import FirebasePerformance
import AnimatedCollectionViewLayout

protocol CanRecieve {
    func dataRecieved(data: [Int])
    
}

private let reuseIdentifier = "CustomViewCell"

class ArticleCollectionViewController: SafariViewController, ArticleReceiver {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sharePerspectivesView: UIView!
    @IBOutlet weak var sharePerspectivesTextField: UITextView!
    @IBOutlet weak var sharePerspectivesViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sharePerspectivesTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sharePerspectiveButtonPressed: UIButton!
    @IBOutlet weak var closeSharePerspectivesView: UIButton!
    
    @IBOutlet weak var messageCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var userCenterXConstraint: NSLayoutConstraint!
    
    var heightAfterKeyboardAdjustment: CGFloat = 0
    var initialMessageTextFieldHeightContraint: CGFloat = 0
    
    var delegate : CanRecieve?
    var countArray : [Int] = [0,0,0,0,0,0]
    var myDictionary: [Int: String] = [:]
    let brevt_URL = "https://api.brevt.com/api"
//    let brevt_URL = "https://dev-api.brevt.com/api"
//    let feedbackPostURL = "https://dev-api.brevt.com/api/feedback/"
    let feedbackPostURL = "https://api.brevt.com/api/feedback/"
    
    var headers: HTTPHeaders = [
        "Authorization": ""
    ]
    
    var newsArticleArray: [NewsArticle] = []
    var savedArticleArray : [String] = []
    var ratingButtonArray: [UIButton] = [UIButton(), UIButton(), UIButton()]
    var labelPassed =  ""
    var tagPassed: Int = 0
    var updatedURL: String = ""
    var jsonData: Any?
    var updatedFeedbackURL = ""
    var numberOfArticlesInResponse: Int = 0
    var passedContentItem: NewsArticle = NewsArticle()
    var reloadedIndexPath: IndexPath?
    var articleIndex: Int = 0
    var passedArticleCounterFromFirebase: [Int] = []
    var counterLabels = ["Science", "Healthcare", "Economics", "Global", "Politics", "Everything"]
    let imageCache = NSCache<AnyObject, AnyObject>()

    func contentReceived(cardArticle: NewsArticle) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = AnimatedCollectionViewLayout()
        layout.animator = PageAttributesAnimator()
       
        collectionView.collectionViewLayout = layout
        collectionView.isPagingEnabled = true
        collectionView.decelerationRate = UIScrollViewDecelerationRateFast

        sharePerspectivesTextField.delegate = self
        sharePerspectivesTextField.adjustsFontForContentSizeCategory = true
        sharePerspectivesTextField.layer.cornerRadius = 5
        sharePerspectivesView.alpha = 0
        initialMessageTextFieldHeightContraint = sharePerspectivesTextFieldHeightConstraint.constant
   
        registerForKeyBoardNotifications()
        retrieveData()
        getNewsData()
        updateUserCount()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        messageCenterXConstraint.constant =  view.frame.width / 3
        userCenterXConstraint.constant =  -view.frame.width / 3
    }
    
    func updateUserCount() {
        guard let currentUser = Auth.auth().currentUser else { return }
        let field = counterLabels[tagPassed - 1]
        let value = passedArticleCounterFromFirebase[tagPassed - 1]
        print("field \(field)")
        print("Value \(value)")
        if value != 0 {
            FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).updateData([field: value]) { (error) in
                print("error if \(String(describing: error))")
            }
        }
        
    }
    
    
   
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dismissKeyBoard()
        sharePerspectivesView.alpha = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToComments" {
            let destinationViewController = segue.destination as! CommentsViewController
            let senderItem = sender as! ArticleCollectionViewController
            let articleItem = senderItem.passedContentItem
            destinationViewController.cardArticle = articleItem
            destinationViewController.delegate = self
            destinationViewController.indexPath = reloadedIndexPath
        }
        
        if segue.identifier == "InviteConversations" {
            let navigation = segue.destination as! UINavigationController
            let destinationViewController = navigation.topViewController as! InviteConversationsViewController
            let senderItem = sender as! ArticleCollectionViewController
            let articleItem = senderItem.passedContentItem
            destinationViewController.delegate = self
            destinationViewController.cardArticle = articleItem
        }
    }
    
    @IBAction func returnToCategoryViewController(_ sender: UIButton) {
        imageCache.removeAllObjects()
        deregisterFromKeyboardNotifications()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func goToUserAccount(_ sender: Any) {
        imageCache.removeAllObjects()
        deregisterFromKeyboardNotifications()
        performSegue(withIdentifier: "unwindToUserAccountVC", sender: self)
        
    }
    
    @IBAction func goToConversations(_ sender: UIButton) {
        imageCache.removeAllObjects()
        deregisterFromKeyboardNotifications()
        performSegue(withIdentifier: "unwindToConversations", sender: self)
        
    }
    
    
    //MARK: Retrieve JSON Response
    func getNewsData()  {
        myDictionary = [1 : "?topic_tags=SCI", 2 : "?topic_tags=HTC", 3 : "?topic_tags=ECN", 4 : "?topic_tags=GLB", 5 : "?topic_tags=POL", 6 : "" ]
        updatedURL = brevt_URL + "/content/" + myDictionary[tagPassed]!
        
        let authTrace = Performance.startTrace(name: "FireBase Auth Trace")
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            
            if error != nil {
//                print("AUTH ERROR GETNEWSDATA: \(error)")
                return;
            }
//            print("TOKEN: \(idToken!)")
            self.headers = ["Authorization": idToken!]
            authTrace?.stop()
            
            let contentRequestTrace = Performance.startTrace(name: "Content Request Trace")
            
            Alamofire.request(self.updatedURL, headers: self.headers).responseJSON {
                response in
                if response.result.isSuccess {
//                    print("Success got news data")
                    contentRequestTrace?.stop()
                    let newsJSON: JSON = JSON(response.result.value!)
                    self.jsonData = newsJSON
                    self.updateNewsData(json: newsJSON)
                } else {
//                    print("error \(String(describing: response.result.error))")
                }
            }
        }
    }
    
    
    // Mark - JSON parsing
    
    func updateNewsData (json: JSON) {
        
        if json["status"].stringValue == "ok" {
            numberOfArticlesInResponse = json["total_results"].intValue
            for i in stride(from: 0, to: numberOfArticlesInResponse , by: 1) {
                let nextJSONArticle = json["articles"][i]
                let articleTitle = nextJSONArticle["title"].stringValue
                let articleURL = nextJSONArticle["source_url"].stringValue
                let articleUniqueID = nextJSONArticle["id"].stringValue
                let newArticleObject = NewsArticle(withTitle: articleTitle, andURL: articleURL, withUniqueID: articleUniqueID)
                
                newArticleObject.description = nextJSONArticle["description"].stringValue
                newArticleObject.urlToImage = nextJSONArticle["image_url"].stringValue
                newArticleObject.ratingScore = nextJSONArticle["rating_score"].doubleValue
            
                newsArticleArray.append(newArticleObject)
            }
            newsArticleArray.reverse()
            collectionView?.reloadData()
        }
    }
    
    // Mark - retrieve saved article array from database
    
    func retrieveData() {
        
        guard let user = Auth.auth().currentUser else {return}
        let docRef = FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("Saved Articles").document("saved articles")
        
        docRef.getDocument{(document, error) in
            if let dataFromUser = document.flatMap({$0.data().flatMap({(data) in
                return SavedArticle(keyValue: data) })
                
            }) {
                self.savedArticleArray = dataFromUser.ArticleInfo
            }
            else {
                //                print("document does not exist")
            }
        }
    }
 
    //Mark - actions from article card
    
    @objc func viewCommentsButtonPressed(sender: UIButton!) {
        let cardIndex = sender.layer.value(forKey: "cardIndex") as! IndexPath
        passedContentItem = newsArticleArray[cardIndex.row]
        reloadedIndexPath = cardIndex
        performSegue(withIdentifier: "goToComments", sender: self)
        
    }
    
    @objc func saveArticlePressed(sender: UIButton!) {
        let index: Int = sender.layer.value(forKey: "index") as! Int
        sender.setBackgroundImage(#imageLiteral(resourceName: "bookmark-green"), for: .normal)
        
        if savedArticleArray.contains(newsArticleArray[index].url) {
            
            let alert = UIAlertController(title: "", message: "Already Saved", preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            
            let when = DispatchTime.now() + 0.5
            DispatchQueue.main.asyncAfter(deadline: when, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        } else {
            // The order of the following append calls is important!
            savedArticleArray.append(newsArticleArray[index].url)
            
            if let urlImage = newsArticleArray[index].urlToImage {
                savedArticleArray.append(urlImage)
            } else {
                // TODO: Add a default image to display if none are assigned
                
            }
            savedArticleArray.append(newsArticleArray[index].title)
            
            guard let user = Auth.auth().currentUser else { return }
            FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("Saved Articles").document("saved articles").setData([
                "ArticleInfo" : savedArticleArray
            ], merge: true){err in
                if err != nil {
                    //                    print("error writing document: \(err)")
                } else {
                    //                    print("Document successfully written!")
                }
            }
        }
        
    }
    
    @IBAction func ratingIndicatorPressed(_ sender: UIButton) {
        let message = "The color reflects the rating that other users have given this article\n ---\n Green: High Rating\n Yellow: Medium Rating\n Red: Low Rating\n ---\n Give the article a read and rate it in the perspective section!"
        let alertController = UIAlertController(title: "Rating Indicator", message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Got It!", style: .default, handler: {action in
            alertController.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func viewArticleInSafari(sender: UIButton!) {
        
        let index: IndexPath = sender.layer.value(forKey: "cardIndex") as! IndexPath
        let cell = self.collectionView.cellForItem(at: index) as! CustomArticleViewCell
        
        UIApplication.shared.open(URL(string: newsArticleArray[index.row].url)!, options: [:], completionHandler: {(status) in
        })
        let when = DispatchTime.now() + 1.3
        DispatchQueue.main.asyncAfter(deadline: when, execute: {
            cell.userOptionsviewHeightContstraint.constant = 182
            cell.userOptionsView.alpha = 1
            
        })
    }
    

    func setRatingLabelColor(forCell cell: CustomArticleViewCell, atIndex index: IndexPath) {
        
        let newsArticle = newsArticleArray[index.row]
        let articleRating = newsArticle.ratingScore
        let highRange = 0.7
        let lowRange = 0.3
        
        let green = UIColor(red: 123.0/255.0, green: 203.0/255.0, blue: 99.0/255.0, alpha: 1.0)
        let yellow = UIColor(red: 241.0/255.0, green: 227.0/255.0, blue: 53.0/255.0, alpha: 1.0)
        let red = UIColor(red: 208.0/255.0, green: 2.0/255.0, blue: 27.0/255.0, alpha: 1.0)
        
        switch articleRating {
        case _ where articleRating! >= highRange:
            cell.ratingIndicator.setTitleColor(green, for: .normal)
        case _ where articleRating! <= lowRange:
            cell.ratingIndicator.setTitleColor(red, for: .normal)
        default:
            cell.ratingIndicator.setTitleColor(yellow, for: .normal)
        }

    }
    

//
// MARK: User Option Selection Menu Functions (i.e. start conversation, share perspective, like/dislike, etc.)
   
    // Opens Menu Option
    
    @objc func showUserOptionMenu(sender: UIButton!) {
        
        let senders = sender
        let index: IndexPath = senders?.layer.value(forKey: "cardIndex") as! IndexPath
        let cell = self.collectionView.cellForItem(at: index) as! CustomArticleViewCell
        
        if cell.userOptionsView.alpha == 0 {
            cell.userOptionsviewHeightContstraint.constant = 182
            UIView.animate(withDuration: 0.2, delay: 0.0,  options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
            
            sharePerspectivesView.alpha = 0
            cell.userOptionsView.alpha = 1
        } else {
            cell.userOptionsviewHeightContstraint.constant = 0
            UIView.animate(withDuration: 0.2, delay: 0.0,  options: .curveEaseOut, animations: {
                cell.userOptionsView.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    
    // User chooses to share a comment/perspective with the BREVT community
    
    @objc func showPerspectivesView(sender: UIButton!) {
        let senders = sender
        let index: IndexPath = senders?.layer.value(forKey: "cardIndex") as! IndexPath
        let cell = self.collectionView.cellForItem(at: index) as! CustomArticleViewCell
        print("test")


        UIView.animate(withDuration: 0.2, delay: 0.0,  options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
            cell.userOptionsView.alpha = 0
            self.sharePerspectivesView.alpha = 1
        }, completion: nil)

        articleIndex = index.row
    }
    
    
    @IBAction func submitPerspectivePressed(_ sender: UIButton!) {
        print("current article index: \(articleIndex)")
        
        let contentItem = newsArticleArray[articleIndex].articleID
        let comment = sharePerspectivesTextField.text
        sharePerspectivesTextField.endEditing(true)
        
        let authTrace = Performance.startTrace(name: "FireBase Auth Trace")
        let currentUser = Auth.auth().currentUser
        if comment != "" {
            currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                if error != nil {
                    //                    print("AUTH ERROR GETNEWSDATA: \(error)")
                    return;
                }
                self.headers = ["Authorization": idToken!]
                authTrace?.stop()
                
                let contentRequestTrace = Performance.startTrace(name: "Content Request Trace")
                let parameters: Parameters = [
                    "comment": "\(comment!)",
                    "content_item": "\(contentItem)",
                    "created_by": "abc123"
                ]
                
                Alamofire.request(self.feedbackPostURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: self.headers ).responseJSON {
                    response in
                        if response.result.isSuccess {
                        
                        contentRequestTrace?.stop()
                        let alert = UIAlertController(title: "", message: "Thank you for sharing your perspective, we'll review it prior to sharing with the BREVT community!", preferredStyle: .alert)
                        
                        self.present(alert, animated: true, completion: nil)
        
                        let when = DispatchTime.now() + 3.0
                        DispatchQueue.main.asyncAfter(deadline: when, execute: {
                            alert.dismiss(animated: true, completion: {

                            })
                        })
                        
                    } else {
                        //                        print("error \(String(describing: response.result.error))")
                    }
                }
            }
            
        } else {
            //            print("no text submitted")
        }
    }
    
    
    @IBAction func closeSharePerspectivesView(_ sender: UIButton!) {
        sharePerspectivesTextField.endEditing(true)
    }
    
    // User chooses to start a conversation with another BREVT user
    
    @objc func startConversationPressed(sender: UIButton!) {
        let cardIndex: IndexPath = sender.layer.value(forKey: "cardIndex") as! IndexPath
        print("button selected")
        passedContentItem = newsArticleArray[cardIndex.row]
        
        let cell = self.collectionView.cellForItem(at: cardIndex) as! CustomArticleViewCell
        
        cell.userOptionsviewHeightContstraint.constant = 0
        cell.userOptionsView.alpha = 0
        
        print("passed Content Item : \(passedContentItem)")
        performSegue(withIdentifier: "InviteConversations", sender: self)
    }
    
    
    // User rates the article
    
    @objc func ratingButtonPressed(sender: UIButton!) {
        
        let buttonTag = sender.tag
        let index = sender.layer.value(forKey: "cardIndex") as! IndexPath
        let score: Double?
        let contentItem = newsArticleArray[index.row]
        
        resetButtonBackground()
        
        switch buttonTag {
        case 0:
            score = 1.0
            sender.setBackgroundImage(#imageLiteral(resourceName: "like-green"), for: .normal)
        case 1:
            score = 0.6
            sender.setBackgroundImage(#imageLiteral(resourceName: "okay-yellow"), for: .normal)
        case 2:
            score = 0.0
            sender.setBackgroundImage(#imageLiteral(resourceName: "dislike-red"), for: .normal)
        default:
            //            print("not a valid tag")
            return
        }
        
        if let score = score {
            postContentRating(forArticle: contentItem, withScore: score)
        } else {
            //            print("Scoring error with \(contentItem.title)")
        }
    }
    
    func resetButtonBackground () {
        ratingButtonArray[0].setBackgroundImage(#imageLiteral(resourceName: "like-grey"), for: .normal)
        ratingButtonArray[1].setBackgroundImage(#imageLiteral(resourceName: "okay-grey"), for: .normal)
        ratingButtonArray[2].setBackgroundImage(#imageLiteral(resourceName: "dislike-grey"), for: .normal)
    }
    
    
    func postContentRating(forArticle article: NewsArticle, withScore score: Double) {
        updatedURL = brevt_URL + "/ratings/"
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if error != nil {
                return;
            }
            //            print("TOKEN: \(idToken!)")
            self.headers = ["Authorization": idToken!]
            
            let parameters: Parameters = [
                "content_item": "\(article.articleID)",
                "rating": "\(score)"
            ]
            
            Alamofire.request(self.updatedURL, method: .post, parameters: parameters, headers: self.headers).responseJSON {
                response in
                if response.result.isSuccess {
                    //                    let newsJSON: JSON = JSON(response.result.value!)
                    //                    debugPrint(newsJSON)
                } else {
                    //                    print("error \(String(describing: response.result.error))")
                }
            }
        }
    }
    
    
}


extension ArticleCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
//    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        print("Prefetched Index PAths: \(indexPaths)")
//        prefechImages(indexPath: indexPaths[0])
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfArticlesInResponse
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomViewCell", for: indexPath) as! CustomArticleViewCell
       
        //Setup for article card
        cell.layer.cornerRadius = 15
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowRadius = 2
        cell.layer.shadowOpacity = 0.9
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.layer.cornerRadius).cgPath
        setRatingLabelColor(forCell: cell, atIndex: indexPath)
        cell.categoryLabel.text = labelPassed
        cell.articleHeadline.text = newsArticleArray[indexPath.row].title

        if let imageFromCache = imageCache.object(forKey: newsArticleArray[indexPath.row].urlToImage! as AnyObject) as? UIImage {
            cell.articleImage.image = imageFromCache
        } else {
            let imageRequestTrace = Performance.startTrace(name: "Image Request Trace")
            Alamofire.request(newsArticleArray[indexPath.row].urlToImage!).responseImage { response in
            imageRequestTrace?.stop()
            if let image = response.result.value {
                let imageToCache = image
                self.imageCache.setObject(imageToCache, forKey: self.newsArticleArray[indexPath.row].urlToImage! as AnyObject)
                cell.articleImage.image = image
                print(image)
                }
            }
        }
        
      
        //Initial user actions setup
        cell.articleSafariViewBTN.layer.setValue(indexPath, forKey: "cardIndex")
        cell.articleSafariViewBTN.addTarget(self, action: #selector(viewArticleInSafari), for: .touchUpInside)
        cell.saveBTN.layer.setValue(indexPath.row, forKey: "index")
        cell.saveBTN.addTarget(self, action: #selector(saveArticlePressed), for: .touchUpInside)
        cell.viewCommentsButton.layer.setValue(indexPath, forKey: "cardIndex")
        cell.viewCommentsButton.addTarget(self, action: #selector(viewCommentsButtonPressed), for: .touchUpInside)
        cell.showUserOptions.layer.setValue(indexPath, forKey: "cardIndex")
        cell.showUserOptions.addTarget(self, action: #selector(showUserOptionMenu), for: .touchUpInside)
        
        // Setup for article rating
        cell.highRatingButton.layer.setValue(indexPath, forKey: "cardIndex")
        cell.highRatingButton.addTarget(self, action: #selector(ratingButtonPressed), for: .touchUpInside)
        cell.lowRatingButton.layer.setValue(indexPath, forKey: "cardIndex")
        cell.lowRatingButton.addTarget(self, action: #selector(ratingButtonPressed), for: .touchUpInside)
        cell.mediumRatingButton.layer.setValue(indexPath, forKey: "cardIndex")
        cell.mediumRatingButton.addTarget(self, action: #selector(ratingButtonPressed), for: .touchUpInside)
        ratingButtonArray.insert(cell.highRatingButton, at: 0)
        ratingButtonArray.insert(cell.mediumRatingButton, at: 1)
        ratingButtonArray.insert(cell.lowRatingButton, at: 2)

        // Setup for starting conversation
        cell.inviteConversationButton.layer.setValue(indexPath, forKeyPath: "cardIndex")
        cell.inviteConversationButton.addTarget(self, action: #selector(startConversationPressed), for: .touchUpInside)

        // Setup for sharing a perspective/comment
        cell.showPerspectivesView.layer.setValue(indexPath, forKey: "cardIndex")
        cell.showPerspectivesView.addTarget(self, action: #selector(showPerspectivesView), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width - 10, height: frameSize.height - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom:10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
}

extension ArticleCollectionViewController: UITextViewDelegate {
        func textViewDidBeginEditing(_ textView: UITextView) {
         closeSharePerspectivesView.alpha = 0.5
    }
    
    func textViewDidChange(_ textView: UITextView) {
        sharePerspectivesTextField = textView
        
        if sharePerspectivesTextFieldHeightConstraint.constant < 80.0 {
            sharePerspectivesTextFieldHeightConstraint.constant =   sharePerspectivesTextField.contentSize.height
        } else {
            sharePerspectivesTextFieldHeightConstraint.constant = 80.0
        }
        
        sharePerspectivesViewHeight.constant = heightAfterKeyboardAdjustment + sharePerspectivesTextFieldHeightConstraint.constant - initialMessageTextFieldHeightContraint
        
        }
    
        func textViewDidEndEditing(_ textView: UITextView) {
        closeSharePerspectivesView.alpha = 0
        sharePerspectivesView.alpha = 0
        sharePerspectivesTextField.text = ""
        sharePerspectivesTextFieldHeightConstraint.constant = 35
    }
    
}

extension ArticleCollectionViewController {
    func registerForKeyBoardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self,  name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self,  name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.sharePerspectivesViewHeight.constant = (keyboardSize?.height)! + 50
        heightAfterKeyboardAdjustment = self.sharePerspectivesViewHeight.constant
        self.view.layoutIfNeeded()
        
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification) {
        UIView.animate(withDuration: 0.5) {
            self.sharePerspectivesViewHeight.constant = 50
            self.heightAfterKeyboardAdjustment = self.sharePerspectivesViewHeight.constant
            self.view.layoutIfNeeded()
        }
    }
    
}












