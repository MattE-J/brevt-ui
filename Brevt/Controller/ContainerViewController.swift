//
//  ContainerViewController.swift
//  Brevt
//
//  Created by Sam Jensen on 4/5/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {
    
    @IBOutlet weak var sideMenuContraint: NSLayoutConstraint!
    
    @IBOutlet weak var SideMenu: UIView!
    var sideMenuOpen = false
    @IBOutlet weak var mainMenu: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
   @objc func toggleSideMenu() {
        if sideMenuOpen{
            view.layoutIfNeeded()
            sideMenuContraint?.constant = -200
            sideMenuOpen = false
            animateMenu()
        } else {
            view.layoutIfNeeded()
            sideMenuContraint?.constant = 0
            sideMenuOpen = true
            animateMenu()
        }
    }
    
    func animateMenu(){
        UIView.animate(withDuration: 0.33, delay: 0.0,  options: .curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)    }
}
