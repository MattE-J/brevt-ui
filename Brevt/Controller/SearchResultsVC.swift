//
//  SearchResultsVC.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Firebase

class SearchResultsVC: UITableViewController {
    
    var results: [BRVTUser] = []
    var articleObject: NewsArticle = NewsArticle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "InviteTableViewCell", bundle: nil), forCellReuseIdentifier: "InviteCell")
        configureTableView()
        
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("see results: \(results)")
        return configureCell(user: results[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        createInvite(indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension SearchResultsVC {
    
    func configureCell(user: BRVTUser) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InviteCell") as! InviteTableViewCell
        cell.profileName.text = user.name
        if let photoLocation = user.photoPath {
            let reference = Storage.storage().reference(withPath: photoLocation)
            cell.profileImage.sd_setImage(with: reference, placeholderImage: #imageLiteral(resourceName: "placeHolderProfileView"))
        } else {
            cell.profileImage.image = #imageLiteral(resourceName: "placeHolderProfileView")
        }
        
        return cell
    }
    
    func createInvite (indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: "Send Initial Message", message:"Send an initial message to start the conversation if the invitee accepts!",  preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "initial message"
        }
        
        let messagAction = UIAlertAction(title: "send invite with message", style: .default) { (_) in
            if alertController.textFields![0].text != "" {
                
                Conversations.inviteWithInitialMessage(userObject: self.results[indexPath.row], articleObject: self.articleObject, messageBody: alertController.textFields![0].text!, completion: { (status, doesConversationExist) in
                    if status == true {
                        self.showAlert(message: "Invite Sent")
                    } else {
                        if status == false && doesConversationExist == 1 {
                            self.showAlert(message: "Conversation Already Started")
                        } else {
                            self.showAlert(message: "Failed to send invite, try again!")
                            
                        }
                    }
                })
                
            } else {
                
                Conversations.invite(userObject: self.results[indexPath.row],  articleObject: self.articleObject) { (status, doesConversationExist) in
                    if status == true {
                        self.showAlert(message: "Invite Sent")
                    } else {
                        if status == false && doesConversationExist == 1 {
                            self.showAlert(message: "Conversation Already Started")
                        } else {
                            self.showAlert(message: "Failed to send invite, try again!")
                        }
                    }
                }
            }
        }
        
        let inviteAction = UIAlertAction(title: "send invite without message", style: .default) { (_) in
            Conversations.invite(userObject: self.results[indexPath.row],  articleObject: self.articleObject) { (status, doesConversationExist) in
                if status == true {
                    self.showAlert(message: "Invite Sent")
                } else {
                    if status == false && doesConversationExist == 1 {
                        self.showAlert(message: "Conversation Already Started")
                    } else {
                        self.showAlert(message: "Failed to send invite, try again!")
                    }
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (_) in
        }
        alertController.addAction(messagAction)
        alertController.addAction(cancelAction)
        alertController.addAction(inviteAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 1.5
        DispatchQueue.main.asyncAfter(deadline: when, execute: {
            alert.dismiss(animated: true, completion: {
                self.dismiss(animated: true, completion: nil)
            })
        })
        
    }
    
}
