//
//  CategoryViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Firebase
import AlamofireImage


class CategoryViewController: SafariViewController, CanRecieve {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categoryViewTabBarItem: UITabBarItem!
    
    var collectionData = ["Science & Technology", "Health", "Business & Economics", "Global", "Politics", "Everything" ]
    var collectionViewImages = [#imageLiteral(resourceName: "ScienceCategory.jpg"), #imageLiteral(resourceName: "health-category.jpg") , #imageLiteral(resourceName: "EconomicsCategory.jpg") , #imageLiteral(resourceName: "GlobalCategory.jpg") , #imageLiteral(resourceName: "PoliticsCategory.png") , #imageLiteral(resourceName: "EverythingCategory.jpg") ]
    var indexArray: [Int] = [0,0,0,0,0,0]
    var featuredArticles: FeaturedArticles?
    var articles: [FeaturedArticle] = []
    var cellCounterValue: [Int] = [0,0,0,0,0,0]
    var articleCounterFromFirebase: [Int] = [0,0,0,0,0,0]
    
    
    private let refreshControl = UIRefreshControl()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveUserData()
        let width = (view.frame.size.width - 20) / 2
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: width)
        
        collectionView.backgroundColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        collectionView.register(HeaderBanner.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        
        
        print("this is users \(Firebase.user.rawValue)")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        articles.removeAll()
        
        FeaturedArticles.fetchFeaturedArticles { (JSON) in
            self.updateFeaturedArticles(json: JSON)
        }
        getArticleCounts()
        
    }
    
    
    func getArticleCounts() {
        
        guard let currentUser = Auth.auth().currentUser else{ return }
        let userRef = FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid)
        userRef.getDocument { (document, error) in
            
            if let document = document {
                let firstTimeSignIn = document.get("counting status")
                if firstTimeSignIn == nil {
                    FirebaseDatabase.sharedInstance.db.collection("Counter").document("ArticleCounter").getDocument(completion: { (document, error) in
                        if let document = document {
                            let setInitialScienceCount = document.get("Science") as! Int
                            let setInitialGlobalCount = document.get("Global") as! Int
                            let setInitialHealthcareCount = document.get("Healthcare") as! Int
                            let setInitialEconomicsCount = document.get("Economics") as! Int
                            let setInitialPoliticsCount = document.get("Politics") as! Int
                            let setInitialEverythingCount = document.get("Everything") as! Int
                            userRef.setData([
                                "counting status" : true,
                                "Science" : setInitialScienceCount,
                                "Economics" : setInitialEconomicsCount,
                                "Global" :  setInitialGlobalCount,
                                "Healthcare" : setInitialHealthcareCount,
                                "Politics" : setInitialPoliticsCount,
                                "Everything" : setInitialEverythingCount,
                                ], merge: true)
                            }
                    })
                } else {
                    
                    self.findFindDifference(usersRef: userRef)
                }
            } else {
                print("error \(String(describing: error))")
            }
        }
    }
    
    func findFindDifference(usersRef: DocumentReference) {
         FirebaseDatabase.sharedInstance.db.collection("Counter").document("ArticleCounter").addSnapshotListener { (document, error) in
            if let document = document {
                let currentScienceCount = document.get("Science") as! Int
                let currentGlobalCount = document.get("Global") as! Int
                let currentHealthcareCount = document.get("Healthcare") as! Int
                let currentEconomicsCount = document.get("Economics") as! Int
                let currentPoliticsCount = document.get("Politics") as! Int
                let currentEverythingCount = document.get("Everything") as! Int
                
                // Order of array is important
                
                self.articleCounterFromFirebase = [currentScienceCount, currentHealthcareCount, currentEconomicsCount, currentGlobalCount, currentPoliticsCount, currentEverythingCount]
                
                usersRef.getDocument(completion: { (document, error) in
                    if let document = document {
                        let usersScienceCount = document.get("Science") as! Int
                        let usersGlobalCount = document.get("Global") as! Int
                        let usersHealthcareCount = document.get("Healthcare") as! Int
                        let usersEconomicsCount = document.get("Economics") as! Int
                        let usersPoliticsCount = document.get("Politics") as! Int
                        let usersEverythingCount = document.get("Everything") as! Int
                        
                        
                        let scienceDifference = currentScienceCount - usersScienceCount
                        let globalDifference = currentGlobalCount - usersGlobalCount
                        let healthCareDifference = currentHealthcareCount - usersHealthcareCount
                        let economicsDifference = currentEconomicsCount - usersEconomicsCount
                        let politicsDifference = currentPoliticsCount - usersPoliticsCount
                        let everyCount = currentEverythingCount - usersEverythingCount
                        
                        // Order of array is important
                        
                        self.cellCounterValue = [scienceDifference, healthCareDifference, economicsDifference, globalDifference, politicsDifference, everyCount]
                        
                        self.collectionView.reloadData()
                    
                    }
                })
                
            }
        }
        
    }
    
    
    func updateFeaturedArticles (json: JSON) {
        
        if json["status"].stringValue == "ok" {
            let numberOfArticlesInResponse = json["total_results"].intValue
            
            for i in stride(from: 0, to: numberOfArticlesInResponse , by: 1) {
                
                let nextJSONArticle = json["articles"][i]
                let newFeaturedArticleObject = FeaturedArticle(articleName: nextJSONArticle["title"].stringValue, articleURL: nextJSONArticle["source_url"].stringValue, imageName: nextJSONArticle["image_url"].stringValue)
                articles.append(newFeaturedArticleObject)
            }
        
            FeaturedArticles.articles = articles
            FeaturedArticles.articles.reverse() 

        }
        collectionView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "goToArticleViewController" {
            if let newsViewVC = segue.destination as? ArticleCollectionViewController,
                let index  = sender as? IndexPath {
                newsViewVC.labelPassed = collectionData[index.row]
                newsViewVC.tagPassed = index.row + 1
                newsViewVC.countArray = indexArray
                newsViewVC.passedArticleCounterFromFirebase = articleCounterFromFirebase
                newsViewVC.delegate = self
                
            }
        }
    }
    
   
    
    func dataRecieved(data: [Int]) {
        indexArray = data
    }
    
    // Mark - Storing user info into user database
    
    func saveUserData(){
        if let user = Auth.auth().currentUser {
            setUserData(forUser: user)
        } else {
            let encodedUser = UserDefaults.standard.object(forKey: "loggedInUser") as! Data
            let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: encodedUser) as! User
//            print("UID OF DECODED USER: \(decodedUser.uid)")
            setUserData(forUser: decodedUser)
        }
    }
    
    func openArticleInSafari(featuredArticle: FeaturedArticle) {
        
        UIApplication.shared.open(URL(string: featuredArticle.articleURL!)!, options: [:], completionHandler: {(status) in
        })
    }
    
    func setUserData(forUser user: User) {
        
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).setData([
            "User email" : user.email ?? "",
            "Userid" : user.uid,
            "User Name": user.displayName ?? ""
     
            ], merge: true)
        {err in
//            if let err = err {
////                print("error writing document: \(err)")
//            } else {
////                print("Document successfully written!")
//            }
        }
    }
    
    @objc private func refreshCollectionView(_ sender: Any) {
        articles.removeAll()
        FeaturedArticles.fetchFeaturedArticles { (JSON) in
            self.updateFeaturedArticles(json: JSON)
        }
        self.collectionView.reloadData()
        self.refreshControl.endRefreshing()
        
    }
    
}

extension CategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath)
        if let label = cell.viewWithTag(100) as? UILabel {
            label.text = collectionData[indexPath.row]
        }
        
        if let counterLabel = cell.viewWithTag(50) as? UILabel {
            if cellCounterValue[indexPath.row] == 0 {
                counterLabel.isHidden = true
            } else {
                counterLabel.isHidden = false
                counterLabel.backgroundColor = UIColor.red
                counterLabel.textAlignment = .center
                counterLabel.layer.cornerRadius = 10
                counterLabel.text = String(cellCounterValue[indexPath.row])
            }
            
        }
        let backroungImage = UIImageView()
        backroungImage.image = collectionViewImages[indexPath.row]
        backroungImage.contentMode = .scaleAspectFill
        backroungImage.layer.masksToBounds = true
        cell.backgroundView = backroungImage
        cell.layer.cornerRadius = 10
        
        collectionView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshCollectionView), for: .valueChanged)

        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("indexPath: \(indexPath)")
        
        
        performSegue(withIdentifier: "goToArticleViewController", sender: indexPath)
        self.collectionView.deselectItem(at: indexPath, animated: true)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 260)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header",  for: indexPath) as! HeaderBanner
        header.categoryViewController = self
        header.bannerCollectionView.isPagingEnabled = true
        header.bannerCollectionView.showsHorizontalScrollIndicator = false
        header.bannerCollectionView.reloadData()
        return header
        
    }
    
}

//MARK: Featured Article Banner Setup

class HeaderBanner: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching{
    
    let cellID = "bannderCellID"
    var categoryViewController = CategoryViewController()
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let bannerLabel: UILabel = {
        let label = UILabel()
        label.text = "Featured Articles "
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        label.translatesAutoresizingMaskIntoConstraints =  false
        label.numberOfLines = 1
        return label
    } ()
    
    let bannerFooter: UILabel = {
        let label = UILabel()
        label.text = "Explore"
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        label.translatesAutoresizingMaskIntoConstraints =  false
        label.numberOfLines = 1
        return label
    } ()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 0
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.currentPageIndicatorTintColor = UIColor.green
        pc.pageIndicatorTintColor = UIColor.white
        return pc
        
    }()
    
    let bannerCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    } ()
    
    func setupViews() {
        
        bannerCollectionView.dataSource = self
        bannerCollectionView.delegate = self
        bannerCollectionView.prefetchDataSource = self
        bannerCollectionView.register(BannerCell.self, forCellWithReuseIdentifier: cellID)
        
        addSubview(bannerCollectionView)
        addSubview(bannerLabel)
        addSubview(bannerFooter)
        addSubview(pageControl)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": bannerLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-150-[v0]-150-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": pageControl]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[v0]-200-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": pageControl]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": bannerFooter]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": bannerCollectionView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[bannerLabel(30)][v0][bannerFooter(30)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": bannerCollectionView, "bannerLabel": bannerLabel, "bannerFooter": bannerFooter]))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = FeaturedArticles.articles.count
        return FeaturedArticles.articles.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! BannerCell
        
        if let imageName = FeaturedArticles.articles[indexPath.item].imageName {
            if let imageFromCache = imageCache.object(forKey: imageName as AnyObject) as? UIImage {
                cell.imageView.image = imageFromCache
            } else {
                Alamofire.request(imageName).responseImage { response  in
                    if let image = response.result.value {
                        cell.imageView.image = image
                        let imageToCache = image
                        self.imageCache.setObject(imageToCache, forKey: imageName as AnyObject)
                    }
                }
            }
        }
        
        if let articleTitle = FeaturedArticles.articles[indexPath.item].title {
            cell.nameLabel.text = "\(articleTitle)"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height - 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        categoryViewController.openArticleInSafari(featuredArticle: FeaturedArticles.articles[indexPath.item] )
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(x / frame.width)
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        prefetchImages(indexPath: indexPaths[0])
    }
    
    func prefetchImages(indexPath: IndexPath) {
        
        print("prefectched indexes for freaturedArticles: \(indexPath.row)")
        
        if let imageName = FeaturedArticles.articles[indexPath.item].imageName {
            if (imageCache.object(forKey: imageName as AnyObject) as? UIImage) != nil {
                print("all ready cached image")
            } else {
                Alamofire.request(imageName).responseImage { response  in
                    if let image = response.result.value {
                        let imageToCache = image
                        self.imageCache.setObject(imageToCache, forKey: imageName as AnyObject)
                    }
                }
                
            }
        }
    }
    
    
}

class BannerCell: UICollectionViewCell {
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image  = nil
        nameLabel.text = nil
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Rectangle")
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        
        return iv
        
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont(name: "AvenirNext-Bold", size: 22)
        label.textColor = UIColor.white
        label.numberOfLines = 0
        
        return label
    }()
    
    func setupViews() {
        
        let view = UIView(frame: CGRect(x: 0, y: frame.height / 2, width: frame.width, height: frame.height / 2))
        
        let gradientLayer: CAGradientLayer = {
            let layer = CAGradientLayer()
            layer.colors = [UIColor.clear.cgColor, UIColor(white: 0.0, alpha: 0.75).cgColor]
            layer.locations = [NSNumber(value: 0.0 as Float), NSNumber(value: 1.0 as Float)]
            return layer
        }()
        
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
        imageView.addSubview(view)
        imageView.bringSubview(toFront: view)
        addSubview(imageView)
        addSubview(nameLabel)
        imageView.frame = CGRect(x: 0, y:0, width: frame.width, height: frame.height)
        nameLabel.frame = CGRect(x: 10, y: 50, width: frame.width - 10, height: frame.height)
        
    }
}
