//
//  SafariViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SafariServices

class SafariViewController: UIViewController, SFSafariViewControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func presentSafariViewController(url: String, inReaderview isReaderView: Bool = true) {
        let svc = SFSafariViewController(url: NSURL(string: url)! as URL)
        svc.delegate = self
        self.present(svc, animated: true, completion: nil)
        
    }
    
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
    }
    func safariViewController(_ controller: SFSafariViewController, activityItemsFor URL: URL, title: String?) -> [UIActivity] {
        return[]
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
    }
    
}

class SafariTableViewController: UITableViewController, SFSafariViewControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func presentSafariViewController(url: String, inReaderview isReaderView: Bool = true) {
        let svc = SFSafariViewController(url: NSURL(string: url)! as URL)
        svc.delegate = self
        self.present(svc, animated: true, completion: nil)
        
    }
    
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
    }
    func safariViewController(_ controller: SFSafariViewController, activityItemsFor URL: URL, title: String?) -> [UIActivity] {
        return[]
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
    }
    
}
