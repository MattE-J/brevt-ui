//
//  WelcomeViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import SVProgressHUD


class WelcomeViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var customGoogleButton: UIButton!
    @IBOutlet weak var emailSignIn: UIButton!
    @IBOutlet weak var emailSignUp: UIButton!
    
    @IBOutlet weak var signInView: UIView!
    @IBOutlet weak var signUpView: UIView!
    
    @IBOutlet weak var startUpLoginView: UIView!
    @IBOutlet var viewController: UIView!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var registerEmail: UITextField!
    @IBOutlet weak var registerPassword: UITextField!
    @IBOutlet weak var brevtLogo: UIImageView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var registerFirstName: UITextField!
    @IBOutlet weak var registerLastName: UITextField!
    
    @IBOutlet weak var resetPasswordView: UIView!
    @IBOutlet weak var resetEmailField: UITextField!
    @IBOutlet weak var resetBackButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupGoogleButtons()
        setGraphics()
        
        startView()
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
    }
    
    
    //Mark: Unwind from log out
    
    @IBAction func unwindLogin (_ segue: UIStoryboardSegue) {
        print("success")
    }
    
    
    // Mark: - Update location of view with keyboard
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        adjustInsetForKeyboardShow(show: true, notification: notification)
        
        WelcomScreenAnimations.moveLogoAnimation(logoImage: brevtLogo, viewController: viewController, flag: true)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        adjustInsetForKeyboardShow(show: false, notification: notification)
        WelcomScreenAnimations.moveLogoAnimation(logoImage: brevtLogo, viewController: viewController, flag: false)
    }
    
    
    func adjustInsetForKeyboardShow(show: Bool, notification: NSNotification) {
        let userInfo = notification.userInfo ?? [:]
        let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let adjustment = (keyboardFrame.height * (show ? 1: 0))
        
        signInView.center.y = show ? (-adjustment + (viewController.bounds.height - 150)) : viewController.bounds.height - 150
        signUpView.center.y = show ? (-adjustment + (viewController.bounds.height - 150)) :  viewController.bounds.height - 150
        resetPasswordView.center.y = show ? (-adjustment + (viewController.bounds.height - 150)) :  viewController.bounds.height - 150
        
    }
    
    
    // Mark: - Sign Up / Sign In with Email transitions
    @IBAction func signInEmail(_ sender: UIButton) {
        WelcomScreenAnimations.startSignUpAnimation(emailView: signInView, startupLoginView: startUpLoginView, viewController: viewController)
    }
    
    @IBAction func signUpEmail(_ sender: UIButton) {
        WelcomScreenAnimations.startSignUpAnimation(emailView: signUpView, startupLoginView: startUpLoginView, viewController: viewController)
        
    }
    
    @IBAction func forgotPasswordPressed(_ sender: UIButton) {
        WelcomScreenAnimations.startSignUpAnimation(emailView: resetPasswordView, startupLoginView: signInView, viewController: viewController)
    }
    
    @IBAction func resetPasswordPressed(_ sender: UIButton) {
        let resetEmailAddress = resetEmailField.text
        let isValidEmail = resetEmailAddress?.isValidEmail()
        resetEmailField.resignFirstResponder()
        
        if isValidEmail! {
//            print("Attempt Reset: \(resetEmailAddress!)")
            Auth.auth().sendPasswordReset(withEmail: resetEmailAddress!) { (error) in
                if error != nil {
//                    debugPrint(error)
                    self.resetEmailField.text = nil
                    self.resetEmailField.placeholder = "Please enter a valid email"
                    self.resetEmailField.layer.borderColor = UIColor.red.cgColor
                    self.resetEmailField.layer.borderWidth = 2.0
                } else {
//                    print("Reset email: \(resetEmailAddress!)")
                    self.goBackFromResetButtonPressed(self.resetBackButton)
                }
            }
            
        } else {
//            print("invalid email: \(resetEmailAddress!)")
            resetEmailField.placeholder = "Please enter a valid email"
            resetEmailField.layer.borderColor = UIColor.red.cgColor
            resetEmailField.layer.borderWidth = 2.0
        }
    }
    
    @IBAction func resetEmailFieldBeganEditing(_ sender: UITextField) {
        sender.layer.borderWidth = 0.0
//        debugPrint("Began editing \(sender)")
    }
    
    //Mark: - Sign Up / Sign In with Email after transitions
    @IBAction func signInButtonPressedAfterTransition(_ sender: Any) {
        SVProgressHUD.show()
        
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (user, error) in
            if error != nil {
                
                SVProgressHUD.setGraceTimeInterval(0.2)
                SVProgressHUD.dismiss()
                
                let alert = UIAlertController(title: "Invalid Login", message:"Please re-enter email and password", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: {
                    (action:UIAlertAction!) -> Void in NSLog ("User pressed OK")
                }))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
//                print("log in successful!")
                SVProgressHUD.dismiss()
                
                self.enablePersistentLogin(forUser: Auth.auth().currentUser!)
                
                self.performSegue(withIdentifier: "goToMainViewController", sender: self)
                
            }
        }
    }
    
    func presentAlert(withMessage message: String, andTitle title: String = ""){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
 
    }
    
    
    @IBAction func signUpButtonPressedAfterTransition(_ sender: UIButton) {
        let enteredFirstName = registerFirstName.text!
        let enteredLastName = registerLastName.text!
        let enteredEmail = registerEmail.text!
//        let enteredPassword = registerPassword.text!
        
        clearBorderColors()
        
        let nameCheckResult = checkNames(firstName: enteredFirstName, lastName: enteredLastName)
        
//        print("First Name Valid: \(nameCheckResult.firstIsValid), Last Name Valid: \(nameCheckResult.lastIsValid)")
//        print("Email Valid: \(enteredEmail.isValidEmail())")
        
        if nameCheckResult.firstIsValid && nameCheckResult.lastIsValid && enteredEmail.isValidEmail() {
            Auth.auth().createUser(withEmail: registerEmail.text!, password: registerPassword.text!){
                (user, error) in
                
                SVProgressHUD.setGraceTimeInterval(0.2)
                SVProgressHUD.show()
                
                
                if error != nil {
                    SVProgressHUD.dismiss()
                    let errorMessage = error?.localizedDescription as String?
//                    print(errorMessage!)
                    self.clearTextFields()
                    self.clearBorderColors()
                    self.presentAlert(withMessage: errorMessage!)
                } else {
//                    print("registration Successful!")
                    SVProgressHUD.dismiss()
                    
                    let updatedUser = Auth.auth().currentUser
                    if let updatedUser = updatedUser {
                        let changeRequest = updatedUser.createProfileChangeRequest()
                        let username = "\(self.registerFirstName.text!) \(self.registerLastName.text!)"
                        changeRequest.displayName = username
                        changeRequest.commitChanges { (error) in
                            if error != nil {
//                                print("error updating user profile display name \(error)")
                            } else {
//                                print("displayname: updated \(updatedUser.uid) to \(updatedUser.displayName!)")
                                self.enablePersistentLogin(forUser: updatedUser)
                                self.performSegue(withIdentifier: "goToMainViewController", sender: self)
                                
                            }
                        }
                    }
                }
            }
        } else {
            var errorMessage = "Please adjust the following fields:"
            if !nameCheckResult.firstIsValid {
                errorMessage.append("\nFirst Name")
                registerFirstName.layer.borderColor = UIColor.red.cgColor
                registerFirstName.layer.borderWidth = 1.0
                registerFirstName.text = ""
            }
            if !nameCheckResult.lastIsValid {
                errorMessage.append("\nLast Name")
                registerLastName.layer.borderColor = UIColor.red.cgColor
                registerLastName.layer.borderWidth = 1.0
                registerLastName.text = ""
            }
            if !enteredEmail.isValidEmail() {
                errorMessage.append("\nEmail Address")
                registerEmail.layer.borderColor = UIColor.red.cgColor
                registerEmail.layer.borderWidth = 1.0
                registerEmail.text = ""
            }
            
            presentAlert(withMessage: errorMessage)
        }
    }
    
    @IBAction func goBackButtonPressedAfterTransition(_ sender: UIButton) {
        reAddObserver()
        view.endEditing(true)
        clearTextFields()
        clearBorderColors()
        WelcomScreenAnimations.returnAnimation(emailView: signUpView, startUpLoginView: startUpLoginView, viewController: viewController)
        WelcomScreenAnimations.returnAnimation(emailView: signInView, startUpLoginView: startUpLoginView, viewController: viewController)
        
    }
    @IBAction func goBackFromResetButtonPressed(_ sender: UIButton) {
        reAddObserver()
        view.endEditing(true)
        clearTextFields()
        clearBorderColors()
        WelcomScreenAnimations.returnAnimation(emailView: resetPasswordView, startUpLoginView: signInView, viewController: viewController)
    }
    
    func reAddObserver() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func clearBorderColors() {
        resetEmailField.layer.borderWidth = 0.0
        registerFirstName.layer.borderWidth = 0.0
        registerLastName.layer.borderWidth = 0.0
        registerEmail.layer.borderWidth = 0.0
        registerPassword.layer.borderWidth = 0.0
    }
    
    func clearTextFields() {
//        print("cleared")
        email.text = nil
        password.text = nil
        registerEmail.text = nil
        registerPassword.text = nil
        registerFirstName.text = nil
        registerLastName.text = nil
        resetEmailField.text = nil
    }
    
    func checkNames(firstName: String, lastName: String) -> (firstIsValid: Bool, lastIsValid: Bool) {
        var lastNameIsValid = false
        var firstNameIsValid = false
        
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z]{2,20}$", options: .caseInsensitive)
            
            if regex.matches(in: firstName, options: [], range: NSMakeRange(0, firstName.count)).count > 0 {firstNameIsValid = true}
            
            if regex.matches(in: lastName, options: [], range: NSMakeRange(0, lastName.count)).count > 0 {lastNameIsValid = true}
        } catch {
//            print("Error checking first/last name")
        }
        
        return (firstIsValid: firstNameIsValid, lastIsValid: lastNameIsValid)
    }
    
    
    
    // Mark:  - google log in functions
    
    func setupGoogleButtons() {
        
        customGoogleButton.addTarget(self, action: #selector(handleCustomGoogleSignIn), for: .touchUpInside)
        
    }
    
    @objc func handleCustomGoogleSignIn() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        SVProgressHUD.show()
        if error != nil {
//            print("Failed to log into Google: ", err)
            SVProgressHUD.dismiss()
            
            let alert = UIAlertController(title: "Login Failed", message:"Failed to login with google, please try again, or sign up with email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: {
                (action:UIAlertAction!) -> Void in NSLog ("User pressed OK")
            }))
            self.present(alert, animated: true, completion: nil)
            
            
            
            return
        }
        
//        print("Successfully logged into Google", user)
        guard let idTokenn = user.authentication.idToken else {return }
        guard let accessTokenn = user.authentication.accessToken else {return}
        let credentials = GoogleAuthProvider.credential(withIDToken: idTokenn, accessToken: accessTokenn)
        
        Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
//            if let err = error {
////                print("failed to create a firebase User with Google account: \(err)")
//            }
            
//            print("Successfully logged into firebase with google \(String(describing: user))")
            
            self.enablePersistentLogin(forUser: user!.user)
            
            SVProgressHUD.dismiss()
            
            self.performSegue(withIdentifier: "goToMainViewController" , sender: self)
            return
            
        }
        
    }
    
    
    
    
    
    // Sets login key to True so user only logs in once
    func enablePersistentLogin(forUser user: User) {
        let userDefaults = UserDefaults.standard
        let encodedUser: Data = NSKeyedArchiver.archivedData(withRootObject: user)
        userDefaults.set(true, forKey: "isUserLoggedIn")
        userDefaults.set(encodedUser, forKey: "loggedInUser")
        userDefaults.synchronize()
    }
    
    // Mark : Configure button graphics
    
    func setGraphics() {
        
        customGoogleButton.layer.cornerRadius = 5
        customGoogleButton.layer.shadowColor = UIColor.gray.cgColor
        customGoogleButton.layer.shadowRadius = 2
        customGoogleButton.layer.shadowOpacity = 0.3
        customGoogleButton.layer.shadowOffset = CGSize(width: 3.0, height: 1.0)
        customGoogleButton.layer.shadowPath = UIBezierPath(rect: customGoogleButton.bounds).cgPath
        
        emailSignIn.layer.cornerRadius = 5
        emailSignIn.layer.shadowColor = UIColor.gray.cgColor
        emailSignIn.layer.shadowRadius = 2
        emailSignIn.layer.shadowOpacity = 0.3
        emailSignIn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        emailSignIn.layer.shadowPath = UIBezierPath(rect: emailSignIn.bounds).cgPath
        
        emailSignUp.layer.cornerRadius = 5
        emailSignUp.layer.shadowColor = UIColor.gray.cgColor
        emailSignUp.layer.shadowRadius = 2
        emailSignUp.layer.shadowOpacity = 0.3
        emailSignUp.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        emailSignUp.layer.shadowPath = UIBezierPath(rect: emailSignUp.bounds).cgPath
        
        signInButton.layer.cornerRadius = 5
        signInButton.layer.shadowColor = UIColor.gray.cgColor
        signInButton.layer.shadowRadius = 2
        signInButton.layer.shadowOpacity = 0.3
        signInButton.layer.shadowOffset = CGSize(width: 3.0, height: 1.0)
        signInButton.layer.shadowPath = UIBezierPath(rect: signInButton.bounds).cgPath
        
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.shadowColor = UIColor.gray.cgColor
        signUpButton.layer.shadowRadius = 2
        signUpButton.layer.shadowOpacity = 0.3
        signUpButton.layer.shadowOffset = CGSize(width: 3.0, height: 1.0)
        signUpButton.layer.shadowPath = UIBezierPath(rect: signUpButton.bounds).cgPath
        
        resetPasswordButton.layer.cornerRadius = 5
        resetPasswordButton.layer.shadowColor = UIColor.gray.cgColor
        resetPasswordButton.layer.shadowRadius = 2
        resetPasswordButton.layer.shadowOpacity = 0.3
        resetPasswordButton.layer.shadowOffset = CGSize(width: 3.0, height: 1.0)
        resetPasswordButton.layer.shadowPath = UIBezierPath(rect: signUpButton.bounds).cgPath
        
        
    }
    
    func startView() {
        startUpLoginView.center.x = viewController.center.x
        startUpLoginView.center.y = viewController.bounds.height - 175
        signInView.center.x = viewController.bounds.width + signInView.bounds.size.width
        signInView.center.y = viewController.bounds.height - 175
        
        signUpView.center.x = viewController.bounds.width + signUpView.bounds.size.width
        signUpView.center.y = viewController.bounds.height - 175
        
        resetPasswordView.center.x = viewController.bounds.width + resetPasswordView.bounds.size.width
        resetPasswordView.center.y = viewController.bounds.height - 175
        
        brevtLogo.center.x = viewController.center.x
        brevtLogo.center.y = viewController.center.y - 100.0
        
        setTextFieldPadding(forTextField: registerFirstName)
        setTextFieldPadding(forTextField: registerLastName)
        setTextFieldPadding(forTextField: registerPassword)
        setTextFieldPadding(forTextField: registerEmail)
        setTextFieldPadding(forTextField: email)
        setTextFieldPadding(forTextField: password)
        setTextFieldPadding(forTextField: resetEmailField)
    }
    
    func setTextFieldPadding(forTextField textField: UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
    }
    
}


class WelcomScreenAnimations {
    
    static func startSignUpAnimation(emailView: UIView, startupLoginView : UIView, viewController: UIView ) {
        
        let moveLeft = CABasicAnimation(keyPath: "position.x")
        let moveLeftFromOffScreen = CABasicAnimation(keyPath: "position.x")
        let changeOpacity = CABasicAnimation(keyPath: "opacity")
        
        moveLeft.duration = 0.3
        moveLeft.fromValue = viewController.center.x
        moveLeft.toValue = viewController.bounds.width - viewController.bounds.width -  startupLoginView.bounds.size.width
        
        changeOpacity.duration = 0.3
        changeOpacity.fromValue = 1.0
        changeOpacity.toValue = 0.0
        
        
        moveLeftFromOffScreen.duration = 0.3
        moveLeftFromOffScreen.fromValue = viewController.bounds.width + emailView.bounds.size.width
        moveLeftFromOffScreen.toValue = viewController.center.x
        
        
        startupLoginView.layer.add(moveLeft, forKey: nil)
        startupLoginView.layer.add(changeOpacity, forKey: nil)
        emailView.layer.add(moveLeftFromOffScreen, forKey: nil)
        
        startupLoginView.center.x = viewController.bounds.width - viewController.bounds.width -  startupLoginView.bounds.size.width
        startupLoginView.layer.opacity = 0.0
        
        emailView.center.x = viewController.center.x
        
        
    }
    
    
    static  func returnAnimation(emailView: UIView, startUpLoginView : UIView, viewController: UIView ) {
        
        let moveRight = CABasicAnimation(keyPath: "position.x")
        let moveRightFromOffScreen = CABasicAnimation(keyPath: "position.x")
        let changeOpacity = CABasicAnimation(keyPath: "opacity")
        
        moveRight.duration = 0.3
        moveRight.fromValue = viewController.center.x
        moveRight.toValue = viewController.bounds.width + emailView.bounds.size.width
        
        changeOpacity.duration = 0.3
        changeOpacity.fromValue = 0.0
        changeOpacity.toValue = 1.0
        
        
        moveRightFromOffScreen.duration = 0.3
        moveRightFromOffScreen.fromValue = viewController.bounds.width - viewController.bounds.width -  startUpLoginView.bounds.size.width
        moveRightFromOffScreen.toValue = viewController.center.x
        
        
        startUpLoginView.layer.add(moveRightFromOffScreen, forKey: nil)
        startUpLoginView.layer.add(changeOpacity, forKey: nil)
        emailView.layer.add(moveRight, forKey: nil)
        
        startUpLoginView.center.x = viewController.center.x
        startUpLoginView.layer.opacity = 1.0
        
        emailView.center.x = viewController.bounds.width + emailView.bounds.size.width
        
    }
    
    static func moveLogoAnimation(logoImage: UIImageView, viewController: UIView, flag: Bool) {
        
        switch flag {
        case true:
            
            let moveUp = CABasicAnimation(keyPath: "position.y")
            moveUp.duration = 0.3
            moveUp.fromValue = viewController.center.y - 100.0
            moveUp.toValue = viewController.bounds.size.height - viewController.bounds.size.height - logoImage.frame.size.height
            logoImage.layer.add(moveUp, forKey: nil)
            logoImage.center.y = viewController.bounds.size.height - viewController.bounds.size.height - logoImage.frame.size.height
            
        case false:
            
            let moveDown = CABasicAnimation(keyPath: "position.y")
            moveDown.duration = 0.6
            moveDown.fromValue = viewController.bounds.size.height - viewController.bounds.size.height - logoImage.frame.size.height
            moveDown.toValue = viewController.center.y - 100.0
            logoImage.layer.add(moveDown, forKey: nil)
            logoImage.center.y = viewController.center.y - 100.0
            
        }
        
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyBoard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
}

extension String {
    func isValidEmail() -> Bool {
        guard !self.lowercased().hasPrefix("mailto:") else { return false }
        guard let emailDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else { return false }
        let matches = emailDetector.matches(in: self, options: NSRegularExpression.MatchingOptions.anchored, range: NSRange(location: 0, length: self.count))
        guard matches.count == 1 else { return false }
        return matches[0].url?.scheme == "mailto"
    }
}
