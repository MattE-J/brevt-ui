//
//  ConversationsViewController.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import SDWebImage

class ConversationsViewController: SafariViewController, ConversationReceiver {
    
    @IBOutlet weak var conversationsTableView: UITableView!
    @IBOutlet weak var invitesTableView: UITableView!
    
    @IBOutlet weak var savedArticleHeadline: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileViewCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var closeProfileViewFromBackGround: UIButton!
    @IBOutlet weak var exitProfileView: UIButton!
    @IBOutlet weak var inboxView: UIView!
    @IBOutlet weak var invitesView: UIView!
    @IBOutlet weak var inboxInvitesSegmentController: UISegmentedControl!
    
    let sectionTitles =  ["Received Invites", "Sent Invites"]
    var coversationDataModel: [ConversationObject] = []
    var invitesDataModel: [[ConversationObject]] = [[], []]
    var conversationObject: ConversationObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeaderView()
        configureTableView()
        
        conversationsTableView.register(UINib(nibName: "ConversationsTableViewCell", bundle: nil), forCellReuseIdentifier: "conversationsCell")
        
        invitesTableView.register(UINib(nibName: "ConversationsTableViewCell", bundle: nil), forCellReuseIdentifier: "conversationsCell")
        
        conversationsTableView.separatorStyle = .none
        invitesTableView.separatorStyle = .none
        
        
        Conversations.observeInvites { (invites) in
            print("invites: \(invites)")
            self.invitesDataModel[0] = invites
            self.invitesTableView.reloadData()
        }
        
        Conversations.observeSentInvites { (sentInvites) in
            print("sent invites: \(sentInvites)")
            self.invitesDataModel[1] = sentInvites
            self.invitesTableView.reloadData()
        }
        
        Conversations.observeConversations { (conversations) in
            print("conversations: \(conversations)")
            self.coversationDataModel = conversations
            self.updateBackgroundLabelConversationsTable()
            
        }
        
        profileViewCenterXConstraint.constant = -view.frame.width/2 - profileView.frame.width/2
        profileView.layer.cornerRadius = 10
        profileImage.asCircle()
        
        invitesView.isHidden = true
        
     
        
    }
    
    func updateBackgroundLabelConversationsTable() {
        if coversationDataModel.count == 0 {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: conversationsTableView.bounds.size.width, height: conversationsTableView.bounds.size.height))
            noDataLabel.text = "No Conversations In Progress"
            noDataLabel.textColor = UIColor.lightGray
            noDataLabel.textAlignment = .center
            conversationsTableView.backgroundView = noDataLabel
        } else {
            conversationsTableView.backgroundView = nil
        }
        conversationsTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).setData(["haveInvitesBeenViewed": true],merge: true, completion: { (error) in
            if let error = error {
                print("error updating document: \(error)")
            } else {
                print("success fully updated user document")
            }
            })
        }
        updateBackgroundLabelConversationsTable()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let user = Auth.auth().currentUser {
            FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).setData(["haveInvitesBeenViewed": true],merge: true, completion: { (error) in
                if let error = error {
                    print("error updating document: \(error)")
                } else {
                    print("success fully updated user document")
                }
            })
        }
    }

    
    @IBAction func switchInboxInvitesView(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            invitesView.isHidden = true
            inboxView.isHidden = false
            updateBackgroundLabelConversationsTable()
        }
        if sender.selectedSegmentIndex == 1 {
            invitesView.isHidden = false
            inboxView.isHidden = true
        }
        
    }
    
    func configureTableView() {
        invitesTableView.rowHeight = UITableViewAutomaticDimension
        invitesTableView.estimatedRowHeight = 150
        conversationsTableView.rowHeight = UITableViewAutomaticDimension
        conversationsTableView.estimatedRowHeight = 150
    }
    
    func configureHeaderView() {
        
        headerView.frame.origin.x = view.safeAreaInsets.left
        headerView.frame.origin.y = view.safeAreaInsets.top
        headerView.frame.size.width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        headerView.frame.size.height = 50.0
        createHeaderShadow(forHeaderView: headerView)
    }
    


    
    @IBAction func returnHome(_ sender: UIButton) {
         performSegue(withIdentifier: "returnToMainViewController", sender: self)
    }
    
    @IBAction func userAccountButtonPressed(_ sender: UIButton) {
         performSegue(withIdentifier: "goToUserAccountView", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChat" {
            if let chatVC = segue.destination as? ChatViewController {
                chatVC.delegate = self
                chatVC.passedConversationObject = conversationObject
            }
            
        }
    }
    
    func contentRecieved(conversationObject: ConversationObject) {
    }
    
    
    @objc func conversationsCellForRowAtAction(sender: UIButton!) {
        
        let indexPath: IndexPath = sender.layer.value(forKey: "conversationsIndexPath") as! IndexPath
        
        conversationsTableView.deselectRow(at: indexPath, animated: true)

        conversationObject = coversationDataModel[indexPath.row]
        
        UserProfile.updateMessageStatus(conversationObject: conversationObject) { (status) in
            if status == true {
                self.conversationsTableView.reloadData()
            }
        }
        performSegue(withIdentifier: "goToChat", sender: self)
        
    }
    
    @objc func invitesCellForRowAtAction(sender: UIButton!) {
        
        let indexPath: IndexPath = sender.layer.value(forKey: "invitesIndexPath") as! IndexPath
        
        if indexPath.section == 0 {
            if let link = URL(string: invitesDataModel[0][indexPath.row].articleURL) {
                UIApplication.shared.open(link, options: [:], completionHandler: nil)
            }
        }
        
        if indexPath.section == 1 {
            if let link = URL(string: invitesDataModel[1][indexPath.row].articleURL) {
                UIApplication.shared.open(link, options: [:], completionHandler: nil)
            }
        }
        
    }
    
    @objc func invitesShowProfileViewAction(sender: UIButton!) {
        let indexPath: IndexPath = sender.layer.value(forKey: "invitesIndexPath") as! IndexPath
    
        let conversationObject = invitesDataModel[indexPath.section][indexPath.row]
        
        UserProfile.retrieveProfile(userID: conversationObject.userID) { (user) in
            if user?.photoPath != nil && user?.photoPath != "" {
                let reference = Storage.storage().reference(withPath: (user?.photoPath!)!)
                self.profileImage.sd_setImage(with: reference, placeholderImage: #imageLiteral(resourceName: "placeHolderProfileView"))
            } else {
                self.profileImage.image = #imageLiteral(resourceName: "placeHolderProfileView")
                
            }
        }
        
        profileName.text = conversationObject.name
        profileViewCenterXConstraint.constant = 0
        closeProfileViewFromBackGround.alpha = 0.5
        UIView.animate(withDuration: 0.33, delay: 0.0,  options: .curveEaseIn, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
    }
    
    @objc func conversationsShowProfileViewAction(sender: UIButton!) {
        let indexPath: IndexPath = sender.layer.value(forKey: "conversationsIndexPath") as! IndexPath
        let conversationObject = coversationDataModel[indexPath.row]
        
        conversationsTableView.deselectRow(at: indexPath, animated: true)
        
        UserProfile.retrieveProfile(userID: conversationObject.userID) { (user) in
            if user?.photoPath != nil && user?.photoPath != "" {
                let reference = Storage.storage().reference(withPath: (user?.photoPath!)!)
                self.profileImage.sd_setImage(with: reference, placeholderImage: #imageLiteral(resourceName: "placeHolderProfileView"))
            } else {
                self.profileImage.image = #imageLiteral(resourceName: "placeHolderProfileView")
                
            }
        }
        
        profileName.text = conversationObject.name
        profileViewCenterXConstraint.constant = 0
        closeProfileViewFromBackGround.alpha = 0.5
        UIView.animate(withDuration: 0.33, delay: 0.0,  options: .curveEaseIn, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
    }
    
    
//    conversationsIndexPath
    
    @IBAction func closeProfileView(_ sender: UIButton) {
        
        profileViewCenterXConstraint.constant = -view.frame.width/2 - profileView.frame.width/2
        
        closeProfileViewFromBackGround.alpha = 0
        
        UIView.animate(withDuration: 0.33, delay: 0.0,  options: .curveEaseIn, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
    
    @IBAction func closeProfileViewFromBackground(_ sender: UIButton) {
        profileViewCenterXConstraint.constant = -view.frame.width/2 - profileView.frame.width/2
        
        closeProfileViewFromBackGround.alpha = 0
        
        UIView.animate(withDuration: 0.33, delay: 0.0,  options: .curveEaseIn, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
        
}



extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count: Int?
        
        if tableView == self.conversationsTableView {
            count = coversationDataModel.count
        }
        if tableView == self.invitesTableView {
            count = invitesDataModel[section].count
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return configureCell(tableView: tableView, indexPath: indexPath)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count: Int?
        
        if tableView == self.invitesTableView {
            count = 2
        }
        
        if tableView == self.conversationsTableView {
            count = 1
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewSection = UIView()
        
        if tableView == self.conversationsTableView {
            return viewSection
        }
        
        if tableView == self.invitesTableView {
            if section == 0 {
                let receivedView = UIView()
                receivedView.backgroundColor = UIColor.white
                let label = UILabel()
                label.text = "Received Invites"
                label.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
                label.textColor = UIColor.darkGray
                label.frame = CGRect(x: 5, y: 0, width: view.frame.width - 5, height: 40)
                receivedView.addSubview(label)
                return receivedView
                
            }
            if section == 1 {
                let sentView = UIView()
                sentView.backgroundColor = UIColor.white
                let label = UILabel()
                label.text = "Sent Invites"
                label.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
                label.textColor = UIColor.darkGray
                label.frame = CGRect(x: 5, y: 0, width: view.frame.width - 5, height: 40)
                sentView.addSubview(label)
                return sentView
            }
        }

        return viewSection
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.invitesTableView {
            return 40
        }
        
        if tableView == self.conversationsTableView {
            return 1
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if tableView == self.invitesTableView {
            if indexPath.section == 0 {
                let declineInviteAction = createDeclineInviteAction(indexPath: indexPath)
                let declineActionConfiguration = UISwipeActionsConfiguration(actions: [declineInviteAction])
                return declineActionConfiguration
            }
            if indexPath.section == 1 {
                if invitesDataModel[1][indexPath.row].invitePendingStatus == true {
                    let uninviteAction = createUninviteAction(indexPath: indexPath)
                    let uninviteActionConfiguration = UISwipeActionsConfiguration(actions: [uninviteAction])
                    return uninviteActionConfiguration
                } else {
                    let clearSentInviteAction = createClearAction(indexPath: indexPath)
                    let clearSentInviteActionConfiguration = UISwipeActionsConfiguration(actions: [clearSentInviteAction])
                    return clearSentInviteActionConfiguration
                }
            }
        }
        
        
        if tableView == self.conversationsTableView {
            
            if coversationDataModel[indexPath.row].conversationStatus == true {
                let endConversationAction = createStopConversationAction(indexPath: indexPath)
                let endConversationActionConfiguration = UISwipeActionsConfiguration(actions: [endConversationAction])
                return endConversationActionConfiguration
            } else {
                let clearConversationAction = createClearConversationAction(indexPath: indexPath)
                let clearConversationActionConfiguration = UISwipeActionsConfiguration(actions: [clearConversationAction])
                return clearConversationActionConfiguration
            }
            
        }
        
        return UISwipeActionsConfiguration()
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if tableView == self.invitesTableView {
            if indexPath.section == 0 {
                let acceptInviteAction = createAcceptInviteAction(indexPath: indexPath)
                let acceptInviteActionConfiguration = UISwipeActionsConfiguration(actions: [acceptInviteAction])
                return acceptInviteActionConfiguration
            }
        }
        
        if tableView == self.conversationsTableView {
            if indexPath.section == 0 {
                let openURLAction = createOpenURLAction(indexPath: indexPath)
                let openURLActionConfiguration = UISwipeActionsConfiguration(actions: [openURLAction])
               
                return openURLActionConfiguration
            }
        }
        
        return UISwipeActionsConfiguration()
    }

}

extension ConversationsViewController {
    
    //Mark : Configure TableView Cell
    
    func configureCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == self.conversationsTableView {

            let cell = tableView.dequeueReusableCell(withIdentifier: "conversationsCell") as! ConversationsTableViewCell
            cell.individualIdentifier.text = "with"
            let conversation = coversationDataModel[indexPath.row]
            cell.articleTitle.text = conversation.articleTitle
            let url = URL(string: conversation.articleImageURL)
            cell.articleImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholderImage"), options: [], completed: nil)
            cell.name.text = conversation.name
            cell.statusLabel.text = ""
            cell.articleView.backgroundColor = UIColor.white
            cell.nameView.backgroundColor = UIColor.white
            
            if conversation.conversationStatus == false {
                cell.nameView.backgroundColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1)
                cell.articleView.backgroundColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1)
                cell.statusLabel.text = "left conversation"
                cell.statusLabel.textColor = UIColor.red
            }
            
    
            if let conversationMessageStatus = conversation.messageStatus {
                if conversationMessageStatus == false {
                    cell.statusLabel.text = "unread messages"
                    cell.statusLabel.textColor = UIColor.blue
                } else {
                    cell.statusLabel.text = ""
                }
                
            }
            
            cell.cellForRowAtAction.layer.setValue(indexPath, forKey: "conversationsIndexPath")
            cell.cellForRowAtAction.addTarget(self, action: #selector(conversationsCellForRowAtAction) , for: .touchUpInside)
            cell.showProfileViewAction.layer.setValue(indexPath, forKey: "conversationsIndexPath")
            cell.showProfileViewAction.addTarget(self, action: #selector(conversationsShowProfileViewAction), for: .touchUpInside)
            
            return cell
        }
        
        if tableView == self.invitesTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "conversationsCell") as! ConversationsTableViewCell
            if indexPath.section == 0 {
                cell.individualIdentifier.text = "from"
                let invite = invitesDataModel[indexPath.section][indexPath.row]
                cell.articleTitle.text = invite.articleTitle
                let url = URL(string: invite.articleImageURL)
                cell.articleImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholderImage"), options: [], completed: nil)
                cell.name.text = invite.name
                
            }
            
            if indexPath.section == 1 {
                cell.individualIdentifier.text = "to"
                let sentIvite = invitesDataModel[indexPath.section][indexPath.row]
                cell.articleTitle.text = sentIvite.articleTitle
                let url = URL(string: sentIvite.articleImageURL)
                
                cell.articleImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholderImage"), options: [], completed: nil)
                cell.name.text = sentIvite.name
                cell.statusLabel.text = "pending..."
                cell.statusLabel.textColor = UIColor(red: 123.0/255.0, green: 203.0/255.0, blue: 99.0/255.0, alpha: 1.0)
                
                if sentIvite.invitePendingStatus == false {
                    cell.nameView.backgroundColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1)
                    cell.articleView.backgroundColor = UIColor(red: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 1)
                    cell.statusLabel.text = "declined"
                    cell.statusLabel.textColor = UIColor.red
                }
            }
            
            cell.cellForRowAtAction.layer.setValue(indexPath, forKey: "invitesIndexPath")
            cell.cellForRowAtAction.addTarget(self, action: #selector(invitesCellForRowAtAction) , for: .touchUpInside)
            cell.showProfileViewAction.layer.setValue(indexPath, forKey: "invitesIndexPath")
            cell.showProfileViewAction.addTarget(self, action: #selector(invitesShowProfileViewAction), for: .touchUpInside)
            
            return cell
        }
        
        return UITableViewCell()

    }
    
    // Mark:  Swipe Action Configurations
    
    func createDeclineInviteAction(indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Decline") { (action, view, completion) in
            Conversations.declineInvite(invite: self.invitesDataModel[0][indexPath.row], completion: { (status) in
                if status == true {
                    completion(true)
                } else {
                    completion(false)
                }
            })
        }
        
        action.backgroundColor = UIColor.red
        return action
        
    }
    
    func createAcceptInviteAction(indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Accept") { (action, view, completion) in
            Conversations.acceptInvite(invite: self.invitesDataModel[0][indexPath.row], completion: { (status) in
                if status == true {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
        }
        action.backgroundColor = UIColor.green
        return action
    }
    
    func createUninviteAction(indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .destructive, title: "Uninvite") { (action, view, completion) in
            Conversations.uninvite(invite: self.invitesDataModel[1][indexPath.row], completion: { (status) in
                if status == true {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
        }
        action.backgroundColor = UIColor.red
        
        return action
    }
    
    func createClearAction(indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .destructive, title: "Remove") { (action, view, completion) in
            Conversations.uninvite(invite: self.invitesDataModel[1][indexPath.row], completion: { (status) in
                if status == true {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
        }
        action.backgroundColor = UIColor.red
        
        return action
    }
    
    func createStopConversationAction(indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .normal, title: "End Conversation") { (action, view, completion) in
            
            let alert = UIAlertController(title: "End Conversation", message:"Are you sure you would like to end the conversation?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                (action:UIAlertAction!) -> Void in NSLog ("User pressed Yes")
                Conversations.stopConversation(conversation: self.coversationDataModel[indexPath.row], completion: { (status) in
                    if status == true {
                        completion(true)
                    }
                    if status == false {
                        completion(false)
                    }
                })
                
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction!) -> Void in NSLog ("User pressed No")
                completion(true)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        action.backgroundColor = UIColor.red
        return action
        
    }
    
    func createClearConversationAction(indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Clear") { (action, view, completion) in
            Conversations.stopConversation(conversation: self.coversationDataModel[indexPath.row], completion: { (status) in
                if status == true {
                    completion(true)
                }
                if status == false {
                    completion(false)
                }
            })
        }
        action.backgroundColor = UIColor.red
        return action
        
    }
    
    func createOpenURLAction(indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Read") { (action, view, completion) in
            if let link = URL(string: self.coversationDataModel[indexPath.row].articleURL) {
                UIApplication.shared.open(link, options: [:], completionHandler: nil)
                completion(true)
            }
        }
        
        action.backgroundColor = UIColor.blue
        return action
    }
    
}
