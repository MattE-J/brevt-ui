//
//  InviteConversationsViewController.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SDWebImage


import UIKit

class InviteConversationsViewController: UIViewController {
    var delegate: ArticleReceiver?
    var cardArticle: NewsArticle = NewsArticle()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let controller = SearchResultsVC()
        controller.articleObject = cardArticle
        let searchController = UISearchController(searchResultsController: controller)
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        
    }
    
    
    @IBAction func returnButtonPressed(_ sender: Any) {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk {
            print("cleared")
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension InviteConversationsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchText = searchController.searchBar.text
        print("\(String(describing: searchText))" )
        UserProfile.searchUsers(text: searchText) { users in
            print("see if completion returning users: \(users)")
            if let controller = searchController.searchResultsController as? SearchResultsVC {
                controller.results = users
                controller.tableView.reloadData()
            }
        }
    }
    
    
}

extension InviteConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        return UITableViewCell()
    }
    
    
}
