//
//  ChatViewController.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SDWebImage

protocol ConversationReceiver {
    func contentRecieved(conversationObject : ConversationObject)
}

class ChatViewController: UIViewController {

    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextField: UITextView!
    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var messageTextFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameOfConvoPartner: UILabel!
    
    var delegate: ConversationReceiver?
    var passedConversationObject: ConversationObject?
    var messageArray: [Message] = [Message]()
    var amountOfLinesToBeShown: CGFloat = 6
    var heightAfterKeyboardAdjustment: CGFloat = 0
    var initialMessageTextFieldHeightContraint: CGFloat = 0
    var loadFlag = true
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTextField.delegate = self
        messageTextField.adjustsFontForContentSizeCategory = true
        messageTextField.layer.cornerRadius = 5
        articleImage.layer.cornerRadius = 5
        articleImage.contentMode = .scaleAspectFill
        articleImage.layer.masksToBounds = true
        
        initialMessageTextFieldHeightContraint = messageTextFieldHeightConstraint.constant
        
        print(initialMessageTextFieldHeightContraint)
        
        registerForKeyBoardNotifications()
        
        messageTableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "MessageCell")
        configureTableView()
        let tapGesure = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        messageTableView.addGestureRecognizer(tapGesure)
        messageTableView.separatorStyle = .none
        
        if let conversationID = passedConversationObject?.conversationID {
            Messages.observeMessages(conversationID: conversationID) { (messages) in
            
                self.messageArray = messages
                self.messageTableView.reloadData()
                
                if self.messageArray.count != 0 && self.loadFlag == true {
                    let indexPath = IndexPath(item: self.messageArray.count - 1, section: 0)
                    print("this is the message array count: \(self.messageArray.count)")
                    self.messageTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    self.loadFlag = false
                }
                
                if self.messageArray.count != 0  && self.loadFlag == false {
                    let indexPath = IndexPath(item: self.messageArray.count - 1, section: 0)
                    print("this is the message array count: \(self.messageArray.count)")
                    self.messageTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        }
        
        if let url = URL(string: passedConversationObject?.articleImageURL ?? "") {
            articleImage.sd_setImage(with:  url, completed: nil)
            
        }
        
        articleTitle.text = passedConversationObject?.articleTitle ?? ""
        nameOfConvoPartner.text = passedConversationObject?.name ?? ""
        
        messageTableView.allowsSelection = false
        
        configureTableView()
        configureHeaderView()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        loadFlag = true
    }
    
   

    func configureHeaderView() {
        
        headerView.frame.origin.x = view.safeAreaInsets.left
        headerView.frame.origin.y = view.safeAreaInsets.top
        headerView.frame.size.width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        createHeaderShadow(forHeaderView: headerView)
    }
    
    @objc func tableViewTapped() {
        messageTextField.endEditing(true)
    }
    
    func configureTableView() {
        messageTableView.estimatedRowHeight = 300.0
        messageTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    @IBAction func returnToConversationVC(_ sender: UIButton) {
        deregisterFromKeyboardNotifications()
        UserProfile.updateMessageStatus(conversationObject: passedConversationObject) { (status) in
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        
        if messageTextField.text != "" && passedConversationObject != nil {
            Messages.sendMessage(converastionObject: passedConversationObject!, messageBody: messageTextField.text!) { (status) in
                if status == true {
                    print("success")
                    self.messageTextField.endEditing(true)
                } else {
                    self.messageTextField.endEditing(true)
                    print("not successful")
                }
            }
            
            self.messageTextField.text = ""
            messageTableView.layoutIfNeeded()
        }
    }

    @IBAction func openArticle(_ sender: Any) {
        if let link = URL(string: (passedConversationObject?.articleURL) ?? "") {
            UIApplication.shared.open(link, options: [:], completionHandler: nil)
        }
    }
    
    func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM d, h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageCell
        
        cell.messageBody.text = messageArray[indexPath.row].messageBody
        
        if let message = messageArray[indexPath.row].timeStamp as? String {
        cell.messageTime.text = UTCToLocal(date: message)
        }
        
  
        if messageArray[indexPath.row].senderID == passedConversationObject?.userID {
            cell.messageBodyBackground.backgroundColor = UIColor(red: 123.0/255.0, green: 203.0/255.0, blue: 99.0/255.0, alpha: 1.0)
            cell.messageBodyBackground.layer.cornerRadius = 5
            cell.messageBody.textColor = UIColor.white
            cell.setConstraints(setUp: true)

        } else {
            cell.messageBodyBackground.backgroundColor = UIColor(red: 211/255.0, green: 210/255.0, blue: 211/255.0, alpha: 1)
            cell.messageBodyBackground.layer.cornerRadius = 5
            cell.messageBody.textColor = UIColor.black
            cell.setConstraints(setUp: false)

        }
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension ChatViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
    }
    
    func textViewDidChange(_ textView: UITextView) {
        messageTextField = textView
        
        if messageTextFieldHeightConstraint.constant < 80.0 {
            messageTextFieldHeightConstraint.constant =   messageTextField.contentSize.height
        } else {
            messageTextFieldHeightConstraint.constant = 80.0
        }
        
        heightConstraint.constant = heightAfterKeyboardAdjustment + messageTextFieldHeightConstraint.constant - initialMessageTextFieldHeightContraint
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        messageTextFieldHeightConstraint.constant = 35
    }
    
}

extension ChatViewController {
    func registerForKeyBoardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self,  name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self,  name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        self.heightConstraint.constant = (keyboardSize?.height)! + 50
        heightAfterKeyboardAdjustment = self.heightConstraint.constant
        self.view.layoutIfNeeded()
        
    }

    @objc func keyboardWillBeHidden(notification: NSNotification) {
        UIView.animate(withDuration: 0.5) {
            self.heightConstraint.constant = 50
            self.heightAfterKeyboardAdjustment = self.heightConstraint.constant
            self.view.layoutIfNeeded()
        }
    }
}
