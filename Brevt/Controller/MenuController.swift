//
//  MenuController.swift
//  Brevt
//
//  Created by Sam Jensen on 4/5/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Firebase

class MenuController: SafariTableViewController {
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setVersionLabel()
    }
    
    func setVersionLabel() {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        let versionString = "Version \(version).\(build)"
        versionLabel.text = versionString
    }
    
    func disablePersistentLogin() {
        UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
        UserDefaults.standard.set(nil, forKey: "loggedInUser")
        UserDefaults.standard.synchronize()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch  indexPath.row {
        case 0:
            super.presentSafariViewController(url: "https://brevt.com/about/", inReaderview: false)
        case 1:
            super.presentSafariViewController(url: "https://brevt.com/about/about-faq/", inReaderview: false)
        case 2:
            super.presentSafariViewController(url: "https://brevt.com/about/community-guidelines/", inReaderview: false)
        case 3:
            super.presentSafariViewController(url: "https://brevt.com/about/content-selection/", inReaderview: false)
        case 4:
            super.presentSafariViewController(url: "https://brevt.com/blog/", inReaderview: false)
        case 5:
            super.presentSafariViewController(url: "https://brevt.com/legal/policies/", inReaderview: false)
        case 6:
            disablePersistentLogin()
            performSegue(withIdentifier: "unwindToLogin", sender: self)
        default:
            break
        }
    }

}
