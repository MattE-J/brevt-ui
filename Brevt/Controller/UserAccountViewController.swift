//
//  UserAccountViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Photos
import SVProgressHUD
import Firebase
import FirebasePerformance
import FirebaseUI
import FirebaseStorage
import SDWebImage
import AlamofireImage

class UserAccountViewController: SafariViewController, UITextFieldDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerSubview: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var confirmNameButton: UIButton!
    @IBOutlet weak var editNameButton: UIButton!
    @IBOutlet weak var cancelNameChangeButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
//    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var savedArticleTable: UITableView!
    @IBOutlet weak var profileView: UIImageView!
   
    
    let picker = UIImagePickerController()
    var userDisplayData: (displayName: String, emailAddress: String)?
    var updateImage = false
    var savedArticleFromUser: [String] = []
    var updatedArticleAfterDeletion: [String] = []
    var urlArray : [String] = []
    var imageArray : [String] = []
    var titleArray : [String] = []
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
    let photoLibraryAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createBackgroundColor()
        setupUserDataTextFields()
        configureTableView()
        picker.delegate = self
        loadProfileImage()
        profileView.asCircle()
        configureHeaderView()
        savedArticleTable.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        savedArticleTable.separatorStyle = .none
        savedArticleFromUser.removeAll()
        updatedArticleAfterDeletion.removeAll()
        urlArray.removeAll()
        imageArray.removeAll()
        titleArray.removeAll()
        getdata()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.setDataAfterDeletion()
        SVProgressHUD.dismiss()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
    func configureHeaderView() {
        headerView.frame.origin.x = view.safeAreaInsets.left
        headerView.frame.origin.y = view.safeAreaInsets.top
        headerView.frame.size.width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        createHeaderShadow(forHeaderView: headerView)
    }
    
    
    
    // MARK: Retrieve User Profile Image
    func loadProfileImage() {
        guard let currentUser = Auth.auth().currentUser else { return }
        let profilePhotoFile = "ProfileImages/" + currentUser.uid
        let reference = Storage.storage().reference(withPath: profilePhotoFile)
        profileView.sd_setImage(with: reference, placeholderImage: #imageLiteral(resourceName: "placeHolderProfileView"))
        
    }

    //MARK: Button press action methods
    
    @IBAction func editNameButtonPressed(_ sender: UIButton) {
        nameTextField.isEnabled = true
        nameTextField.isUserInteractionEnabled = true
        nameTextField.updateFocusIfNeeded()
        nameTextField.backgroundColor = UIColor.white
        nameTextField.becomeFirstResponder()
        
        editNameButton.isHidden = true
        confirmNameButton.isHidden = false
        cancelNameChangeButton.isHidden = false
    }
    
    
    @IBAction func confirmNameButtonPressed(_ sender: UIButton) {
        let newDisplayName = nameTextField.text
        
        if newDisplayName == "" {
            nameTextField.layer.borderColor = UIColor.red.cgColor
            //email text field red
        } else {
            makeFirebaseDisplayNameChange(withName: newDisplayName!)
            
        }
    }
    
    @IBAction func cancelNameChangeButtonPressed(_ sender: UIButton) {
        setupUserDataTextFields()
    }

    
    @IBAction func menuButtonTapped(_ sender: UIButton) {

        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        if updateImage {
            UIView.transition(with: menuButton, duration: 0.33, options: .transitionCrossDissolve, animations: {
                self.menuButton.setImage(#imageLiteral(resourceName: "bars"), for: .normal)            }, completion: nil)
            updateImage = false
        } else {
        menuButton.setImage(#imageLiteral(resourceName: "ExitFilled"), for: .normal)
            UIView.transition(with: menuButton, duration: 0.33, options: .transitionCrossDissolve, animations: {
                self.menuButton.setImage(#imageLiteral(resourceName: "ExitFilled"), for: .normal)            }, completion: nil)
            updateImage = true
        }
    }
    
    @IBAction func presentPhotoActionSheet(_ sender: AnyObject) {
        selectPhotoOptionSheet()
    }

    //MARK: Configuration/display setup methods
    
    func setupUserDataTextFields() {
        nameTextField.delegate = self
        nameTextField.backgroundColor = UIColor.clear
        nameTextField.textColor = UIColor.black
        nameTextField.isEnabled = false
        nameTextField.isUserInteractionEnabled = false

        
        editNameButton.isHidden = false
        confirmNameButton.isHidden = true
        cancelNameChangeButton.isHidden = true
        
        userDisplayData = getUserDataFromFirebase()
        let userDisplayName = userDisplayData?.displayName

        if userDisplayName == "" {
            nameTextField.textColor = UIColor.gray
            nameTextField.text = "Add your name!"
        } else {
            nameTextField.text = userDisplayName
        }
        

        
    }
    
    
    //MARK: Supporting methods
    func getUserDataFromFirebase() -> (displayName: String, emailAddress: String) {
        var displayName = ""
        var displayEmail = ""
        
        if let currentUser = Auth.auth().currentUser {
            if let name = currentUser.displayName {
                displayName = name
            }
            
            if let email = currentUser.email {
                displayEmail = email
            }
        }
        
        return (displayName, displayEmail)
    }
    
    func makeFirebaseDisplayNameChange(withName newDisplayName: String) {
        let currentUser = Auth.auth().currentUser
        
        let changeRequest = currentUser?.createProfileChangeRequest()
        
        changeRequest?.displayName = newDisplayName
        
        changeRequest?.commitChanges{ (error) in
            if error != nil {
                self.setupUserDataTextFields()
            } else {
                self.setupUserDataTextFields()
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document((currentUser?.uid)!).setData([
                    "User Name": currentUser?.displayName ?? ""
                    ], merge: true)
            }
        }
    }
    
    func createBackgroundColor() {
        backgroundView.backgroundColor = UIColor(red: 90/255.0, green: 215/255.0, blue: 90/255.0, alpha: 0.95)
    }
}

// MARK: Set-up saved article tableview

extension UserAccountViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomNewsViewTableViewCell" , for: indexPath) as! NewsViewTableViewCell
        let headlineFontSize = cell.articleTitle.font.pointSize
        cell.articleTitle.font = UIFont(name: "AvenirNext-DemiBold", size: headlineFontSize)
        tableView.separatorStyle = .none
        cell.articleImage.layer.cornerRadius = 5
        cell.articleImage.contentMode = .scaleAspectFill
        cell.articleImage.layer.masksToBounds = true
        cell.articleTitle.text = titleArray[indexPath.row]

        Alamofire.request(imageArray[indexPath.row]).responseImage { response  in
            if let image = response.result.value {
                cell.articleImage.image = image
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         UIApplication.shared.open(URL(string: urlArray[indexPath.row])!, options: [:], completionHandler: {(status) in
        })
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewSavedArticleHeader = UIView()
        viewSavedArticleHeader.backgroundColor = UIColor.white
        let label = UILabel()
        label.text = "Saved Articles"
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 18)
        label.textColor = UIColor.black
        label.frame = CGRect(x: 5, y: 0, width: view.frame.width - 5, height: 40)
        viewSavedArticleHeader.addSubview(label)

        return viewSavedArticleHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let clearConversationAction = createClearAction(indexPath: indexPath)
        let clearConversationActionConfiguration = UISwipeActionsConfiguration(actions: [clearConversationAction])
        return clearConversationActionConfiguration
        
    }
    // Mark: - Deletion Methods
    
    func createClearAction(indexPath: IndexPath) -> UIContextualAction {
        
        let action = UIContextualAction(style: .destructive, title: "Remove") { (action, view, completion) in
            self.urlArray.remove(at: indexPath.row)
            self.imageArray.remove(at: indexPath.row)
            self.titleArray.remove(at: indexPath.row)
            self.updateBackgroundTableView()
            
        }
        action.backgroundColor = UIColor.red
        
        return action
    }

    func setDataAfterDeletion() {
        for i in stride(from: 0, to: urlArray.count, by: 1){
            updatedArticleAfterDeletion.append(urlArray[i])
            updatedArticleAfterDeletion.append(imageArray[i])
            updatedArticleAfterDeletion.append(titleArray[i])
            
        }
        updateDB()
    }
    
    func updateDB() {
        guard let user = Auth.auth().currentUser else {return }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("Saved Articles").document("saved articles").updateData([
            "ArticleInfo" : updatedArticleAfterDeletion
            ]
        )
    }
    
    // Mark: - retrieving user saved articles from firestore
    
    func getdata(){
        guard let user = Auth.auth().currentUser else { return }
        let docRef = FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("Saved Articles").document("saved articles")
        SVProgressHUD.setGraceTimeInterval(0.5)
        SVProgressHUD.show()
        
        docRef.getDocument { (document, error) in
            if let retrievedData = document.flatMap({ $0.data().flatMap({ (data)  in
                return SavedArticle(keyValue: data)
            })
                
            }) { self.savedArticleFromUser = retrievedData.ArticleInfo
                self.setNewArrays()
                SVProgressHUD.dismiss()
                self.updateBackgroundTableView()
                self.configureTableView()
            }
            else {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func updateBackgroundTableView() {
        
        if urlArray.count == 0 {
            
            let noDataLabel = BackgroundLabel(frame: CGRect(x: 0, y: 0, width: savedArticleTable.bounds.size.width, height: savedArticleTable.bounds.size.height))
            noDataLabel.text = "No Saved Articles"
            noDataLabel.textColor = UIColor.lightGray
            noDataLabel.textAlignment = .center
        
            savedArticleTable.backgroundView = noDataLabel
        } else {
            savedArticleTable.backgroundView = nil
        }
        savedArticleTable.reloadData()
    }
    
    // Mark: - orgainizing array into URL Array, Image Array, and Title array
    
    func setNewArrays() {
        for i in stride(from: 0, to: savedArticleFromUser.count, by: 3){
            urlArray.append(savedArticleFromUser[i])
        }
        for i in stride(from: 1, to: savedArticleFromUser.count, by: 3){
            imageArray.append(savedArticleFromUser[i])
        }
        for i in stride(from: 2, to: savedArticleFromUser.count, by: 3){
            titleArray.append(savedArticleFromUser[i])
        }
    }
    
    func configureTableView() {
        savedArticleTable.estimatedRowHeight = 180
        savedArticleTable.rowHeight = UITableViewAutomaticDimension
    }
}

//MARK: Image Picker Methods

extension UserAccountViewController {
    
    func selectPhotoOptionSheet() {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let PhotoFromLibrary = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            switch self.photoLibraryAuthorizationStatus {
            case .authorized : self.selectImageFromLibrary()
            case .denied : self.photoLibraryAccessNeeded()
            case .notDetermined : self.requestPhotoLibraryPermission()
            default: break
            }
            print("Photo Library Selected")
        })
        
        let takePhotoSelected = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            switch self.cameraAuthorizationStatus {
            case .notDetermined: self.requestCameraPermission()
            case .authorized: self.takePhoto()
            case .denied: self.cameraAccessNeeded()
            default: break
            }
        

            print("Take Photo Selected")
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            print("Cancelled")
        })

        optionMenu.addAction(PhotoFromLibrary)
        optionMenu.addAction(takePhotoSelected)
        optionMenu.addAction(cancelAction)

        self.present(optionMenu, animated: true, completion: nil)
    
    }
    
    
    // Camera Methods
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video) { (accessGranted) in
            guard accessGranted == true else {return}
            self.takePhoto()
        }
    }
    
    func cameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)
        let alert = UIAlertController(title: "Need Camera Access", message: "Camera access is required to take a photo for your profile", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) in
            UIApplication.shared.open(settingsAppURL!, options: [:], completionHandler: nil)
        }))
        
        print("test")
        present(alert, animated: true, completion:  nil)
        
    }
    
    func takePhoto() {
        self.picker.allowsEditing = false
        self.picker.sourceType = UIImagePickerControllerSourceType.camera
        self.picker.cameraCaptureMode = .photo
        self.picker.modalPresentationStyle = .fullScreen
        self.present(self.picker, animated: true, completion: nil)
    }
    
    //Photo Library Methods
    
    func requestPhotoLibraryPermission() {
        PHPhotoLibrary.requestAuthorization { (accessGranted) in
            guard accessGranted == PHAuthorizationStatus.authorized else {return}
            self.selectImageFromLibrary()
        }
    }
    
    func selectImageFromLibrary(){
        self.picker.allowsEditing = false
        self.picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(self.picker, animated: true, completion: nil)
    }
    
    func photoLibraryAccessNeeded(){
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)
        let alert = UIAlertController(title: "Need Photo Library Access", message: "Photo Library access is required to select a photo for your profile", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Access", style: .cancel, handler: { (alert) in
            UIApplication.shared.open(settingsAppURL!, options: [:], completionHandler: nil)
        }))
        present(alert, animated: true, completion:  nil)
    }
}

extension UserAccountViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else{ return }
        print("image details: \(image)")
        UserProfile.saveProfileImage(image: image) { (status) in
            if status == true {
                print("success saved image and user photoURL: \(String(describing: Auth.auth().currentUser?.photoURL))")
                self.profileView.image = image
            } else {
                print("failed to upload user photo")
                let alert = UIAlertController(title: "Failed to upload photo", message: "Check your connection", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion:  nil)
            }
        }
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk {
            print("cleared")
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension UIImageView {
    func asCircle() {
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.masksToBounds = true
    }
}

class BackgroundLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
     
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsetsMake(270, 0, 0, 0)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }
    
}
