//
//  TabBarController.swift
//  Brevt
//
//  Created by Sam Jensen on 6/1/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet weak var mainTabBar: UITabBar!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.selectedIndex = 1
        mainTabBar.tintColor = UIColor(red: 123.0/255.0, green: 203.0/255.0, blue: 99.0/255.0, alpha: 1.0)
        mainTabBar.items![0].imageInsets = UIEdgeInsets(top: 7, left: 0, bottom: -7, right: 0)
        mainTabBar.items![1].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        mainTabBar.items![2].imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        
        mainTabBar.isTranslucent = false
        mainTabBar.backgroundColor = UIColor.white
        mainTabBar.layer.borderWidth = 0.0
        mainTabBar.clipsToBounds = true
        
        
        
        UserProfile.observeUserStatus { (status) in
            if status == true {
                self.mainTabBar.items![2].badgeValue = ""
                
            } else {
                self.mainTabBar.items![2].badgeValue = nil

            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func unwindToConversationsVC (_ segue: UIStoryboardSegue) {
        self.selectedIndex = 2
    }
    
    @IBAction func unwindToUserAccountVC (_ segue: UIStoryboardSegue) {
        self.selectedIndex = 0
    }
}
