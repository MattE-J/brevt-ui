//
//  CommentsViewController.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage
import Alamofire
import SVProgressHUD
import Firebase
import FirebasePerformance

protocol ArticleReceiver {
    func contentReceived(cardArticle: NewsArticle)

}

class CommentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: ArticleReceiver?
    var cardArticle: NewsArticle = NewsArticle()
    
    
    @IBOutlet var commentTableView: UITableView!
    
    
//    @IBOutlet weak var headerShadowView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var headerTitle: UILabel!
    var gradientLayer: CAGradientLayer!
    var indexPath: IndexPath? 
    
    var commentsArray: [FeedbackComment] = [FeedbackComment]()
    
    let API_URL_DEV = "https://dev-api.brevt.com/"
    let API_URL_PROD = "https://api.brevt.com/"
    
    var requestHeaders: HTTPHeaders = [
        "Authorization": ""
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("Loaded article: \(cardArticle.articleID)")
        
        commentTableView.delegate = self
        commentTableView.dataSource = self
        commentTableView.register(UINib(nibName: "FeedbackCell", bundle: nil), forCellReuseIdentifier: "customFeedbackCell")
        
        configureTableView()
        retrieveComments()
        commentTableView.separatorStyle = .none
        createGradientLayer()
        
        commentTableView.tableFooterView = UIView() //hides excess lines
        
        
    }
    
    func createGradientLayer() {
        
        headerView.frame.origin.x = view.safeAreaInsets.left
        headerView.frame.origin.y = view.safeAreaInsets.top
        headerView.frame.size.width = view.bounds.width - view.safeAreaInsets.left - view.safeAreaInsets.right
        headerView.frame.size.height = 50.0
        
        createHeaderShadow(forHeaderView: headerView)
        
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feedbackCell = tableView.dequeueReusableCell(withIdentifier: "customFeedbackCell", for: indexPath) as! CustomFeedbackCell
        
        feedbackCell.layer.cornerRadius = 15
        feedbackCell.layer.masksToBounds = false
        feedbackCell.feedbackMessage.text = commentsArray[indexPath.row].comment
        feedbackCell.feedbackUsername.text = commentsArray[indexPath.row].createdByDisplayName
        tableView.separatorStyle = .singleLine
        
        return feedbackCell
    }
    
    
    
    func configureTableView() {
        commentTableView.rowHeight = UITableViewAutomaticDimension
        commentTableView.estimatedRowHeight = 150.0
    }
    
    func retrieveComments() {
        
        
//        let updatedURL = API_URL_DEV + "api/feedback/?content_item=" + cardArticle.articleID
        let updatedURL = API_URL_PROD + "api/feedback/?content_item=" + cardArticle.articleID
        let currentUser = Auth.auth().currentUser
        
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if error != nil {
//                print("AUTH ERROR Retrieving Comments: \(error)")
                return;
            }
            
//            print("TOKEN: \(idToken!)")
            
            SVProgressHUD.setGraceTimeInterval(1.0)
            SVProgressHUD.show()
            
            self.requestHeaders = ["Authorization": idToken!]
            
            Alamofire.request(updatedURL, headers: self.requestHeaders).responseJSON {
                response in
                if response.result.isSuccess {
//                    print("Success got comments for \(updatedURL)")
                    
                    let feedbackJSON: JSON = JSON(response.result.value!)
                    let totalNumberOfComments: Int = feedbackJSON["total_results"].intValue
                    let responseStatus: String = feedbackJSON["status"].stringValue
                    //                    let responseFeedbackArray = feedbackJSON["feedback"]
                    
//                    print("Total num of comments: \(totalNumberOfComments)")
                    if responseStatus == "ok" && totalNumberOfComments > 0 {
                        for i in stride(from: 0, to: totalNumberOfComments, by: 1) {
                            let feedbackItem = feedbackJSON["feedback"][i]
//                            debugPrint(feedbackItem)
                            
                            let newFeedbackComment = FeedbackComment(forContentItem: feedbackItem["content_item"].stringValue)
                            newFeedbackComment.feedbackID = feedbackItem["id"].stringValue
                            newFeedbackComment.comment = feedbackItem["comment"].stringValue
                            newFeedbackComment.qualityScore = feedbackItem["quality_score"].doubleValue
                            newFeedbackComment.createdByUID = feedbackItem["created_by"].stringValue
                            newFeedbackComment.createdByDisplayName = feedbackItem["display_name"].stringValue
                            newFeedbackComment.createdOnDate = feedbackItem["created_on"].stringValue
                            newFeedbackComment.modifiedOnDate = feedbackItem["modified_on"].stringValue
                            
                            self.commentsArray.append(newFeedbackComment)
                            self.configureTableView()
                            DispatchQueue.main.async {
                                self.commentTableView.reloadData()
//                                print("prolly updated table view data")
                            }
                            
                            SVProgressHUD.dismiss()
                        }
                    } else {
//                        print("Bad Request, status not OK or no comments \(totalNumberOfComments)")
                        let defaultFeedbackComment = FeedbackComment(forContentItem: "Default")
                        defaultFeedbackComment.comment = "No perspectives have been reviewed yet! We evaluate them against our community guidelines as quickly as possible!"
                        defaultFeedbackComment.createdByDisplayName = ""
                        
                        self.commentsArray.append(defaultFeedbackComment)
                        self.configureTableView()
                        DispatchQueue.main.async {
                            self.commentTableView.reloadData()
//                            print("prolly updated table view data")
                        }
                        
                        SVProgressHUD.dismiss()
                        
                    }
                } else {
//                    print("Request error \(String(describing: response.result.error))")
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func exitButtonPressed(_ sender: UIButton) {
        SVProgressHUD.dismiss()
        
       
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension UIViewController {
    func createHeaderShadow(forHeaderView parentView: UIView) {
        let shadowView: UIView = UIView()
        shadowView.backgroundColor = UIColor.white
        parentView.addSubview(shadowView)
        
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.leadingAnchor.constraint(equalTo: parentView.leadingAnchor).isActive = true
        shadowView.trailingAnchor.constraint(equalTo: parentView.trailingAnchor).isActive = true
        shadowView.topAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
        shadowView.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        shadowView.layer.shadowRadius = 1.0
        shadowView.layer.shadowOpacity = 0.1
        
        parentView.bringSubview(toFront: shadowView)
        
    }
}
