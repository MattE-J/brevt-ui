//
//  ViewController.swift
//  Brevt
//
//  Created by Sam Jensen on 6/3/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
