//
//  FirebaseFunctions.swift
//  Brevt
//
//  Created by Sam Jensen on 5/24/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

import Foundation
import Firebase
import FirebaseStorage
import SDWebImage

class FirebaseDatabase {
    
    static let sharedInstance  = FirebaseDatabase()
    let db = Firestore.firestore()
    
    private init() {}
}


// Mark: Set User Profile & Search for usersTest
class UserProfile {
    
    class func saveProfileImage(image: UIImage, completion: @escaping (Bool) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {completion(false); return }
        let profilePhotoFile = currentUser.uid
        let storageRef = Storage.storage().reference().child("ProfileImages").child(profilePhotoFile)
        guard let data = UIImageJPEGRepresentation(image, 0.05) else{completion(false); return}

        storageRef.putData(data, metadata: nil) {  (metadata, error) in
            if error != nil {
                print("This did not work as expected")
                print("error uploading data: \(String(describing: error))")
                completion(false)
            } else {
                let profileObject: [String: Any] = [
                    "UserProfilePhotoStorageLocation" : storageRef.fullPath,
                    ]
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).updateData(profileObject)
                completion(true)
            }
        }
    }
    
    class func searchUsers(text: String?, completion: @escaping ([BRVTUser]) -> Void) {
        guard let text = text else { completion([]); return}
        
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).order(by: "User Name").start(at: [text]).end(at: [text + "~"]).limit(to: 10).getDocuments { (snapshot, error) in
            if let err = error {
                print("error getting documents: \(err)")
                completion([])
            } else {
                
                var optionalUserDictionaries: [[String: Any]] =  []
                for document in (snapshot?.documents)! {
                    optionalUserDictionaries.append(document.data())
                }
                print("optional user dictionaries: \(optionalUserDictionaries)")
                var foundusersTest = optionalUserDictionaries.compactMap({ (data) -> BRVTUser? in
                    return BRVTUser.init(userDictionary: data)
                })
                
                guard let currentUser = Auth.auth().currentUser else { completion(foundusersTest); return}
                for (index, user) in foundusersTest.enumerated() {
                    if user.userID == currentUser.uid {
                        foundusersTest.remove(at: index)
                    }
                }
                completion(foundusersTest)
                
            }
        }
    }
        
    
    class func retrieveProfile(userID: String, completion: @escaping (BRVTUser?) -> Void) {
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userID).getDocument { (document, error) in
            if let err = error {
                print("error getting document: \(err)")
                completion((nil))
            } else{
                let foundUser = BRVTUser.init(userDictionary: document?.data())
                completion(foundUser)
                
                
            }
        }
    }
    
    class func observeUserStatus (completion: @escaping(Bool) -> Void) {
        
        guard let currentUser = Auth.auth().currentUser else { return }
        
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).addSnapshotListener { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                return
            }
            if let haveInvitesBeenViewed = document.get("haveInvitesBeenViewed") {
                if haveInvitesBeenViewed as! Bool == false{
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
        
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").whereField("Message Status", isEqualTo: false).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot else {
                print("error retrieving snapshot: \(String(describing: error))")
                return
            }
            if documents.isEmpty {
                print("testing")
                completion(false)
                
            } else {
                completion(true)
            }
        }
        
        completion(false)
        
        
    }
    

    class func updateMessageStatus(conversationObject: ConversationObject?, completion: @escaping(Bool) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {completion(false); return}
        
        if conversationObject != nil {
            if let conversationID = conversationObject!.conversationID {
            FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").document(conversationID).setData(["Message Status" : true], merge: true) { (error) in
                if let error = error{
                    print("error updating message status \(error)")
                } else {
                    print("Message status updated correctly")
                    }
                }
            }
        }
    }
    
}


//Mark: Set/Listen to Conversations

class Conversations {
    class func invite(userObject: BRVTUser, articleObject: NewsArticle, completion: @escaping(Bool, Int) -> Void) {
        guard let currentUser = Auth.auth().currentUser else { completion(false, 0); return}
        print("current user uid: \(currentUser.uid)")
        print("invited indiviual user: \(userObject.userID)")
        
        let docRef = FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").document("\(userObject.userID + currentUser.uid + articleObject.articleID)")
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                print("conversation may ave already been started")
                let status = document.get("Status") as! Bool
                if status == true {
                    print("conversation in progress")
                    completion(false, 1)
                } else {
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).collection("RecievedInvites").document("\(currentUser.uid + articleObject.articleID)").setData([
                        "User Name" : currentUser.displayName ?? "",
                        "Userid"  : currentUser.uid,
                        "Article ID" : articleObject.articleID,
                        "Article URL" : articleObject.url,
                        "Article ImageURL" : articleObject.urlToImage ?? "",
                        "Article Title" : articleObject.title,
                        "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                        "Status" : true
                        
                    ]) { err in
                        if let err = err {
                            print("error adding document: \(err)")
                            completion(false, 0)
                            
                        } else {
                            print("document successfully written ")
                        }
                    }
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("SentInvites").document("\(userObject.userID + articleObject.articleID)").setData([
                        "User Name" : userObject.name,
                        "Userid"  : userObject.userID,
                        "Article ID" : articleObject.articleID,
                        "Article URL" : articleObject.url,
                        "Article ImageURL" : articleObject.urlToImage ?? "",
                        "Article Title" : articleObject.title,
                        "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                        "Status" : true,
                        "Invite Pending" : true
                    ]) { err in
                        if let err = err {
                            print("error adding document: \(err)")
                            completion(false, 0)
                        } else {
                            print("document successfully written ")
                            completion(true, 0)
                        }
                    }
                    
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).setData(["haveInvitesBeenViewed": false],merge: true, completion: { (error) in
                        if let error = error {
                            print("error updating document: \(error)")
                        } else {
                            print("success fully updated user document")
                        }
                    })
                }
            } else {
                print("conversation has not been started")
                
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).collection("RecievedInvites").document("\(currentUser.uid + articleObject.articleID)").setData([
                    "User Name" : currentUser.displayName ?? "",
                    "Userid"  : currentUser.uid,
                    "Article ID" : articleObject.articleID,
                    "Article URL" : articleObject.url,
                    "Article ImageURL" : articleObject.urlToImage ?? "",
                    "Article Title" : articleObject.title,
                    "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                    "Status" : true
                    
                ]) { err in
                    if let err = err {
                        print("error adding document: \(err)")
                        completion(false, 0)
                    } else {
                        print("document successfully written ")
                    }
                }
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("SentInvites").document("\(userObject.userID + articleObject.articleID)").setData([
                    "User Name" : userObject.name,
                    "Userid"  : userObject.userID,
                    "Article ID" : articleObject.articleID,
                    "Article URL" : articleObject.url,
                    "Article ImageURL" : articleObject.urlToImage ?? "",
                    "Article Title" : articleObject.title,
                    "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                    "Status" : true,
                    "Invite Pending" : true
                ]) { err in
                    if let err = err {
                        print("error adding document: \(err)")
                        completion(false, 0)
                    } else {
                        print("document successfully written ")
                        completion(true, 0)
                    }
                    
                }
                
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).setData(["haveInvitesBeenViewed": false],merge: true, completion: { (error) in
                    if let error = error {
                        print("error updating document: \(error)")
                    } else {
                        print("success fully updated user document")
                    }
                })
                
            }
            
            
        }
        
    }
    
    
    
    class func inviteWithInitialMessage (userObject: BRVTUser, articleObject: NewsArticle, messageBody: String, completion: @escaping(Bool, Int) -> Void) {
        guard let currentUser = Auth.auth().currentUser else { completion(false, 0); return}
        print("current user uid: \(currentUser.uid)")
        print("invited indiviual user: \(userObject.userID)")
        
        let date = Date();
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let utcTimeZoneStr = formatter.string(from: date)
        
        let docRef = FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").document("\(userObject.userID + currentUser.uid + articleObject.articleID)")
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                print("conversation may ave already been started")
                let status = document.get("Status") as! Bool
                if status == true {
                    print("conversation in progress")
                    completion(false, 1)
                } else {
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).collection("RecievedInvites").document("\(currentUser.uid + articleObject.articleID)").setData([
                        "User Name" : currentUser.displayName ?? "",
                        "Userid"  : currentUser.uid,
                        "Article ID" : articleObject.articleID,
                        "Article URL" : articleObject.url,
                        "Article ImageURL" : articleObject.urlToImage ?? "",
                        "Article Title" : articleObject.title,
                        "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                        "Status" : true,
                        
                    ]) { err in
                        if let err = err {
                            print("error adding document: \(err)")
                            completion(false, 0)
                        } else {
                            print("document successfully written ")
                        }
                    }
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("SentInvites").document("\(userObject.userID + articleObject.articleID)").setData([
                        "User Name" : userObject.name,
                        "Userid"  : userObject.userID,
                        "Article ID" : articleObject.articleID,
                        "Article URL" : articleObject.url,
                        "Article ImageURL" : articleObject.urlToImage ?? "",
                        "Article Title" : articleObject.title,
                        "Status" : true,
                        "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                        "Invite Pending" : true
                        
                    ]) { err in
                        if let err = err {
                            print("error adding document: \(err)")
                            completion(false, 0)
                        } else {
                            print("document successfully written ")
                        }
                    }
                    
                    FirebaseDatabase.sharedInstance.db.collection("conversations").document(userObject.userID + currentUser.uid + articleObject.articleID).collection("Messages").addDocument(data: [
                        "MessageBody": messageBody,
                        "Sender" : Auth.auth().currentUser?.displayName ?? "",
                        "TimeStamp": utcTimeZoneStr,
                        "SenderID" : Auth.auth().currentUser?.uid ?? "",
                        
                    ]) { err in
                        if let err = err {
                            print("error adding document: \(err)")
                            completion(false, 0)
                        } else {
                            print("document successfully written ")
                            completion(true, 0)
                        }
                    }
                    
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).setData(["haveInvitesBeenViewed": false],merge: true, completion: { (error) in
                        if let error = error {
                            print("error updating document: \(error)")
                        } else {
                            print("success fully updated user document")
                        }
                    })
        
                }
            } else {
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).collection("RecievedInvites").document("\(currentUser.uid + articleObject.articleID)").setData([
                    "User Name" : currentUser.displayName ?? "",
                    "Userid"  : currentUser.uid,
                    "Article ID" : articleObject.articleID,
                    "Article URL" : articleObject.url,
                    "Article ImageURL" : articleObject.urlToImage ?? "",
                    "Article Title" : articleObject.title,
                    "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                    "Status" : true
                ]) { err in
                    if let err = err {
                        print("error adding document: \(err)")
                        completion(false, 0)
                    } else {
                        print("document successfully written ")
                    }
                    
                }
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("SentInvites").document("\(userObject.userID + articleObject.articleID)").setData([
                    "User Name" : userObject.name,
                    "Userid"  : userObject.userID,
                    "Article ID" : articleObject.articleID,
                    "Article URL" : articleObject.url,
                    "Article ImageURL" : articleObject.urlToImage ?? "",
                    "Article Title" : articleObject.title,
                    "Status" : true,
                    "UserProfilePhotoStorageLocation" : userObject.photoPath ?? "",
                    "Invite Pending" : true
                    
                    
                    
                ]) { err in
                    if let err = err {
                        print("error adding document: \(err)")
                        completion(false, 0)
                    } else {
                        print("document successfully written ")
                    }
                    
                }
  
                FirebaseDatabase.sharedInstance.db.collection("conversations").document(userObject.userID + currentUser.uid + articleObject.articleID).collection("Messages").addDocument(data: [
                    "MessageBody": messageBody,
                    "Sender" : Auth.auth().currentUser?.displayName ?? "",
                    "TimeStamp": utcTimeZoneStr,
                    "SenderID" : Auth.auth().currentUser?.uid ?? "",
                    
                ]) { err in
                    if let err = err {
                        print("error adding document: \(err)")
                        completion(false, 0)
                    } else {
                        print("document successfully written ")
                        completion(true, 0)
                    }
                }
                
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(userObject.userID).setData(["haveInvitesBeenViewed": false],merge: true, completion: { (error) in
                    if let error = error {
                        print("error updating document: \(error)")
                    } else {
                        print("success fully updated user document")
                    }
                })
                
            }
        }
    }
    
    
    class func observeInvites(completion: @escaping ([ConversationObject]) -> Void) {
        guard let user = Auth.auth().currentUser else { completion([]); return}
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("RecievedInvites").whereField("Status", isEqualTo: true).addSnapshotListener { (snapshot, error) in
            
            guard let documents = snapshot?.documents else {
                print("error fetching documents: \(String(describing: error))")
                return
            }
            var optionalConversationInviteDictionaries: [[String: Any]] = []
            
            for document in documents {
                
                optionalConversationInviteDictionaries.append(document.data())
            }
            
            let foundInvites = optionalConversationInviteDictionaries.compactMap({ (data) -> ConversationObject? in
                return ConversationObject.init(conversationDictionary: data)
            })
            
            
            completion(foundInvites)
            
        }
        
    }
    
    class func observeSentInvites(completion: @escaping ([ConversationObject]) -> Void) {
        
        guard let user = Auth.auth().currentUser else {completion([]); return}
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(user.uid).collection("SentInvites").whereField("Status", isEqualTo: true).addSnapshotListener { (snapshot, error) in
            guard let documents = snapshot?.documents else {
                print("error fetching documents: \(String(describing: error))")
                completion([])
                return
            }
            var optionalConversationSentDictionaries: [[String: Any]] = []
            
            for document in documents {
                optionalConversationSentDictionaries.append(document.data())
            }
            
            let foundSentInvites = optionalConversationSentDictionaries.compactMap({ (data) -> ConversationObject? in
                return ConversationObject.init(conversationDictionary: data)
            })
            
            completion(foundSentInvites)
            
            
        }
        
        
    }
    
    class func observeConversations(completion: @escaping ([ConversationObject]) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {completion([]); return }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").whereField("Status", isEqualTo: true).addSnapshotListener { (snapshot, error) in
            guard let documents  = snapshot?.documents else {
                print("error fetching documents: \(String(describing: error))")
                completion([])
                return
            }
            var optionalConversationsDictionaries: [[String: Any]] = []
        
            for document in documents {
                optionalConversationsDictionaries.append(document.data())
            }
            
            let foundConversations = optionalConversationsDictionaries.compactMap({ (data) -> ConversationObject? in
                return ConversationObject.init(conversationDictionary: data)
            })
            completion(foundConversations)
        }
        
    }
    
    class func declineInvite(invite: ConversationObject, completion: @escaping(Bool) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {completion(false); return}
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("RecievedInvites").document(invite.userID + invite.articleID).updateData(
        ["Status": false]) {
            err in
            if let err = err {
                print("error updating document: \(err)")
                completion(false)
            } else {
                print("Document successfully update")
                
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(invite.userID).collection("SentInvites").document(currentUser.uid + invite.articleID).updateData([
                    "Invite Pending" : false]
                ) {
                    err in
                    if let err = err {
                        print("error updating document: \(err)")
                        completion(false)
                    } else {
                        print("Document successfully update")
                        completion(true )
                    }
                }
            }
        }
    }
    
    class func acceptInvite(invite: ConversationObject, completion: @escaping(Bool) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {completion(false); return }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").document("\(currentUser.uid + invite.userID + invite.articleID)").setData([
            "User Name" : invite.name,
            "Userid"  : invite.userID,
            "Article ID" :  invite.articleID,
            "Article URL" : invite.articleURL,
            "Article ImageURL" :  invite.articleImageURL,
            "Article Title" : invite.articleTitle,
            "Status" : true,
            "ConversationStatus" : true,
            "Conversation Identifier" : currentUser.uid + invite.userID + invite.articleID ,
            
            ]) { err in
                if let err = err {
                    print("error adding document: \(err)")
                } else {
                    print("document successfully written ")
                }
                
        }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(invite.userID).collection("Conversations").document("\(currentUser.uid + invite.userID + invite.articleID)").setData([
            "User Name" : currentUser.displayName ?? "",
            "Userid"  : currentUser.uid,
            "Article ID" : invite.articleID,
            "Article URL" : invite.articleURL,
            "Article ImageURL" : invite.articleImageURL,
            "Article Title" : invite.articleTitle,
            "Status" : true,
            "ConversationStatus" : true,
            "Conversation Identifier" : currentUser.uid + invite.userID + invite.articleID,
            
            
            ]) { err in
                if let err = err {
                    print("error adding document: \(err)")
                } else {
                    print("document successfully written ")
                }
                
        }
        
        FirebaseDatabase.sharedInstance.db.collection("conversations").document("\(currentUser.uid + invite.userID + invite.articleID)").setData(
            [
                "invitedByName" : invite.name,
                "invitedByID" : invite.userID,
                "acceptedByName" : currentUser.displayName ?? "",
                "acceptedByID" : currentUser.uid
            ]
            
        )
        
        
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("RecievedInvites").document(invite.userID + invite.articleID).updateData(["Status" : false]) { error in
            if let error = error {
                print("error updating document: \(error)")
            } else {
                
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(invite.userID).collection("SentInvites").document(currentUser.uid + invite.articleID).updateData(["Status" : false]) { error in
                    if let error = error {
                        print("error updating document: \(error)")
                    } else {
                        completion(true)
                    }
                }
            }
        }
        
    }
    
    class func uninvite(invite: ConversationObject, completion: @escaping(Bool) -> Void) {
        
        guard let currentUser = Auth.auth().currentUser else {completion(false); return }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("SentInvites").document(invite.userID + invite.articleID).updateData([
            "Status" : false]
        ) {
            err in
            if let err = err {
                print("error updating document: \(err)")
                completion(false)
            } else {
                FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(invite.userID).collection("RecievedInvites").document(currentUser.uid + invite.articleID).updateData(["Status" : false]) {error in
                    if let error = error {
                        print("error updating document: \(error)")
                    }
                    else {
                        completion(true)
                    }
                }
            }
            
        }
    }
    
    class func stopConversation(conversation: ConversationObject, completion: @escaping(Bool) -> Void) {
        guard let currentUser = Auth.auth().currentUser else{ completion(false); return }
        if let conversationID = conversation.conversationID {
            FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(currentUser.uid).collection("Conversations").document(conversationID).updateData(["Status": false]) { error in
                if let error = error {
                    print("error updating document: \(error)")
                    completion(false)
                } else {
                    FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(conversation.userID).collection("Conversations").document(conversationID).updateData(["ConversationStatus": false]) { error in
                        if let error = error {
                            print("error updating document: \(error)")
                            completion(false)
                        } else {
                            completion(true)
                        }
                    }
                }
            }
        }
    }
}

//Mark: Creation/Listen to Message Objects

class Messages {
    class func sendMessage(converastionObject: ConversationObject, messageBody: String, completion: @escaping(Bool) -> Void) {
        
        let date = Date();
        let formatter = DateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        //let defaultTimeZoneStr = formatter.string(from: date)
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let utcTimeZoneStr = formatter.string(from: date)
        
        if let conversationID = converastionObject.conversationID {
            FirebaseDatabase.sharedInstance.db.collection("conversations").document(conversationID).collection("Messages").addDocument(data: [
                "MessageBody": messageBody,
                "Sender" : Auth.auth().currentUser?.displayName ?? "",
                "TimeStamp": utcTimeZoneStr,
                "SenderID" : Auth.auth().currentUser?.uid ?? "",
                
            ]) { err in
                if let err = err {
                    print("error adding document: \(err)")
                    completion(false)
                } else {
                    print("document successfully written ")
                    completion(true)
                }
            }
        FirebaseDatabase.sharedInstance.db.collection(Firebase.user.rawValue).document(converastionObject.userID).collection("Conversations").document(conversationID).setData(["Message Status": false], merge: true) { (error) in
            if error != nil {
                    print("error updaing message status field")
                } else{
                    print("successfully updated message status field")
                }
            }

        } else {
            completion(false)
        }
        
    }
    
    class func observeMessages(conversationID: String, completion: @escaping([Message]) -> Void) {
        
        print("Conversation ID: \(conversationID)")
        
        FirebaseDatabase.sharedInstance.db.collection("conversations").document(conversationID).collection("Messages").order(by: "TimeStamp", descending: false).addSnapshotListener { (snapshot, error) in
            guard let documents = snapshot?.documents else {
                print("error fetching documents: \(String(describing: error))")
                completion([])
                return
            }
            
            var messageDictionaries: [[String: Any]] = []
            
            for document in documents {
                
                messageDictionaries.append(document.data())
            }
            
            let foundMessages = messageDictionaries.compactMap({ (data) -> Message? in
                return Message.init(messageDictionary: data)
            })

            completion(foundMessages)
            
        }
    }
}
