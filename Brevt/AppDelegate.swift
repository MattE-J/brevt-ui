//
//  AppDelegate.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        CFMobile.initialize("fU4ZiUfoSuaXyTum");

        let userIsLoggedIn = UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        self.window = UIWindow(frame: UIScreen.main.bounds)

        var nvC = NavigationController()
        nvC = storyboard.instantiateViewController(withIdentifier: "NavigationController") as! NavigationController
        self.window?.rootViewController = nvC
        
        if userIsLoggedIn {
            let tabBarVC = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            UserDefaults.standard.synchronize()
            nvC.pushViewController(tabBarVC, animated: true)
        } else {
            let welcomeVC = storyboard.instantiateViewController(withIdentifier: "WelcomeView")
            nvC.pushViewController(welcomeVC, animated: true)
        }
        self.window?.makeKeyAndVisible()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return handled
    }
}
