//
//  CustomFeedbackCell.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class CustomFeedbackCell: UITableViewCell {
    
    @IBOutlet var feedbackBackground: UIView!
    @IBOutlet var feedbackUsername: UILabel!
    @IBOutlet var feedbackMessage: UILabel!
    @IBOutlet weak var discussButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
