//
//  CustomArticleViewCell.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit


class CustomArticleViewCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var articleView: UIView!
    @IBOutlet weak var articleHeadline: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleSafariViewBTN: UIButton!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var viewCommentsButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ratingIndicator: UIButton!
    
    @IBOutlet weak var showUserOptions: UIButton!
    @IBOutlet weak var userOptionsView: UIView!
    @IBOutlet weak var userOptionsviewHeightContstraint: NSLayoutConstraint!
    @IBOutlet weak var showPerspectivesView: UIButton!
    @IBOutlet weak var inviteConversationButton: UIButton!
 
    @IBOutlet weak var ratingsView: UIView!
    @IBOutlet weak var highRatingButton: UIButton!
    @IBOutlet weak var lowRatingButton: UIButton!
    @IBOutlet weak var mediumRatingButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.articleImage.layer.cornerRadius = 5
        
        setFontStyle()
        setRatingsButtons()
        setShowUsersOptionsView()
    }
    

    override func prepareForReuse() {
        super.prepareForReuse()
        
        saveBTN.setBackgroundImage(#imageLiteral(resourceName: "bookmark-black"), for: .normal)
        articleImage.image = nil
    
        setFontStyle()
        setRatingsButtons()
        setShowUsersOptionsView()
    }
    
    func setFontStyle() {
        let headlineFontSize = articleHeadline.font.pointSize
        let categoryLabelFontSize = categoryLabel.font.pointSize

        articleHeadline.font = UIFont(name: "AvenirNext-DemiBold", size: headlineFontSize)
        categoryLabel.font = UIFont(name: "AvenirNext-DemiBold", size: categoryLabelFontSize)

        ratingIndicator.setTitle("\u{2B24}", for: .normal)
        ratingIndicator.titleLabel?.font = ratingIndicator.titleLabel?.font.withSize(30.0)
    }
    
    func setRatingsButtons() {
        highRatingButton.setBackgroundImage(#imageLiteral(resourceName: "like-grey"), for: .normal)
        mediumRatingButton.setBackgroundImage(#imageLiteral(resourceName: "okay-grey"), for: .normal)
        lowRatingButton.setBackgroundImage(#imageLiteral(resourceName: "dislike-grey"), for: .normal)
    }
    
    func setShowUsersOptionsView(){
        userOptionsView.alpha = 0
        userOptionsviewHeightContstraint.constant = 0
    }
    
}
