//
//  InviteTableViewCell.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class InviteTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.asCircle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        profileImage.image = nil
        
    }
    
}
