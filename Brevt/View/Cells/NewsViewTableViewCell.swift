//
//  TableViewCell.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit


class NewsViewTableViewCell: UITableViewCell{
    
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    
    override func prepareForReuse() {
        articleImage.image = nil
        
    }
}
