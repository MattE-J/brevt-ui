//
//  MessageCell.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var messageTextView: UIView!
    @IBOutlet weak var messageBodyBackground: UIView!
    @IBOutlet weak var messageBody: UILabel!
    @IBOutlet weak var messageTime: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        messageBody.lineBreakMode = .byWordWrapping
        messageBody.numberOfLines = 0
    }
    
    override func prepareForReuse() {
        messageBodyBackground.removeFromSuperview()
        messageTime.removeFromSuperview()
        messageTextView.addSubview(messageBodyBackground)
        messageTextView.addSubview(messageTime)
        messageBodyBackground.addSubview(messageBody)
    }
    
    func setConstraints(setUp: Bool) {

        messageTextView.translatesAutoresizingMaskIntoConstraints = false
        messageBodyBackground.translatesAutoresizingMaskIntoConstraints = false
        messageBody.translatesAutoresizingMaskIntoConstraints = false
        messageTime.translatesAutoresizingMaskIntoConstraints = false

        messageTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 5).isActive = true
        messageTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5).isActive = true
        messageTextView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
        messageTextView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5).isActive = true

        
        if setUp{
        messageBodyBackground.leadingAnchor.constraint(equalTo: messageTextView.leadingAnchor, constant: 5).isActive = true
        messageBodyBackground.trailingAnchor.constraint(lessThanOrEqualTo: messageTextView.trailingAnchor, constant: -30).isActive = true
        messageBodyBackground.topAnchor.constraint(equalTo: messageTextView.topAnchor, constant: 0).isActive = true
        messageBodyBackground.bottomAnchor.constraint(equalTo: messageTime.topAnchor, constant: 0).isActive = true
            
        messageTime.leadingAnchor.constraint(equalTo: messageTextView.leadingAnchor, constant: 5).isActive = true
        messageTime.trailingAnchor.constraint(lessThanOrEqualTo: messageTextView.trailingAnchor, constant: -30).isActive = true
        messageTime.topAnchor.constraint(equalTo: messageBodyBackground.bottomAnchor, constant: 0).isActive = true
        messageTime.bottomAnchor.constraint(equalTo: messageTextView.bottomAnchor, constant: 0).isActive = true
            
        } else{
        
        messageBodyBackground.leadingAnchor.constraint(greaterThanOrEqualTo: messageTextView.leadingAnchor, constant: 30).isActive = true
        messageBodyBackground.trailingAnchor.constraint(equalTo: messageTextView.trailingAnchor, constant: -5).isActive = true
        messageBodyBackground.topAnchor.constraint(equalTo: messageTextView.topAnchor, constant: 0).isActive = true
        messageBodyBackground.bottomAnchor.constraint(equalTo: messageTime.topAnchor, constant: 0).isActive = true
            
        messageTime.leadingAnchor.constraint(greaterThanOrEqualTo: messageTextView.leadingAnchor, constant: 30).isActive = true
        messageTime.trailingAnchor.constraint(equalTo: messageTextView.trailingAnchor, constant: -5).isActive = true
        messageTime.topAnchor.constraint(equalTo: messageBodyBackground.bottomAnchor, constant: 0).isActive = true
        messageTime.bottomAnchor.constraint(equalTo: messageTextView.bottomAnchor, constant: 0).isActive = true
            
        }
        
        messageBody.leadingAnchor.constraint(equalTo: messageBodyBackground.leadingAnchor, constant: 5).isActive = true
        messageBody.trailingAnchor.constraint(equalTo: messageBodyBackground.trailingAnchor, constant: -5).isActive = true
        messageBody.topAnchor.constraint(equalTo: messageBodyBackground.topAnchor, constant: 5).isActive = true
        messageBody.bottomAnchor.constraint(equalTo: messageBodyBackground.bottomAnchor, constant: -5).isActive = true
    }
    
    
}
