//
//  ConversationsTableViewCell.swift
//  Brevt
//
//  Created by Sam Jensen on 5/25/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit

class ConversationsTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var individualIdentifier: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var showProfileViewAction: UIButton!
    @IBOutlet weak var cellForRowAtAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        articleImage.layer.cornerRadius = 5
        articleImage.contentMode = .scaleAspectFill
        articleImage.layer.masksToBounds = true
    }
 
    override func prepareForReuse() {
        articleView.backgroundColor = UIColor.white
        nameView.backgroundColor = UIColor.white
        statusLabel.text = nil
    }
    
    
}
