//
//  ConversationModel.swift
//  Brevt
//
//  Created by Sam Jensen on 5/24/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

class ConversationObject {
    let name: String
    let userID: String
    let articleImageURL: String
    let articleTitle: String
    let articleURL: String
    let articleID: String
    let invitePendingStatus: Bool?
    let conversationID: String?
    let conversationStatus: Bool?
    let messageStatus: Bool?
    
    init?(conversationDictionary: [String: Any]?) {
        guard let name = conversationDictionary?["User Name"] as? String,
            let userID = conversationDictionary?["Userid"] as? String,
            let articleImageURL = conversationDictionary?["Article ImageURL"] as? String,
            let articleTitle = conversationDictionary?["Article Title"] as? String,
            let articleURL = conversationDictionary?["Article URL"] as? String,
            let articleID = conversationDictionary?["Article ID"] as? String else {
                return nil
        }
        
        self.conversationStatus = conversationDictionary?["ConversationStatus"] as? Bool
        self.invitePendingStatus = conversationDictionary?["Invite Pending"] as? Bool
        self.conversationID = conversationDictionary?["Conversation Identifier"] as? String
        self.messageStatus = conversationDictionary?["Message Status"] as? Bool
        self.name = name
        self.userID = userID
        self.articleImageURL = articleImageURL
        self.articleTitle = articleTitle
        self.articleURL = articleURL
        self.articleID = articleID
        
    }
}
