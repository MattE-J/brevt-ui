//
//  FeaturedArticles.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import SwiftyJSON

struct FeaturedArticles {
    static var articles: [FeaturedArticle] = []
    
    static func fetchFeaturedArticles(_ completionHandler: @escaping (JSON) -> ()) {
        
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            
            if error != nil {
                return;
            }
            let headers: HTTPHeaders  = ["Authorization": idToken!]
            let featured_URL  = "https://api.brevt.com/api/content/?featured=True"
            
            Alamofire.request(featured_URL, headers: headers).responseJSON {
                response in
                if response.result.isSuccess {
                    let featuredArticleJSON: JSON = JSON(response.result.value!)

                    DispatchQueue.main.async(execute: { () -> Void in
                        completionHandler(featuredArticleJSON)
                    })
                }
            }
        }
    }
}

struct FeaturedArticle {
    var title: String?
    var imageName: String?
    var articleURL: String?
    
    init(articleName: String, articleURL: String, imageName: String) {
        self.title = articleName
        self.articleURL = articleURL
        self.imageName  = imageName
    }
    
}
