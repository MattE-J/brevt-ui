//
//  UserModel.swift
//  Brevt
//
//  Created by Sam Jensen on 5/24/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

class BRVTUser {
    let name: String
    let photoPath: String?
    let userID: String
    let userEmail: String
    
    init?(userDictionary: [String: Any]?) {
        guard let name = userDictionary?["User Name"] as? String,
            let userID = userDictionary?["Userid"] as? String,
            let userEmail = userDictionary?["User email"] as? String else {
                return nil
        }
        self.name = name
        self.photoPath = userDictionary?["UserProfilePhotoStorageLocation"] as? String
        self.userID = userID
        self.userEmail = userEmail
        
    }
}
