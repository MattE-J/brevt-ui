//
//  SavedArticle.swift
//  Brevt
//
//  Created by Sam Jensen on 5/24/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

struct SavedArticle  {
    
    var ArticleInfo: [String]
    
    init?(keyValue: [String:Any]) {
        guard let ArticleInfo = keyValue["ArticleInfo"] as? [String] else { return nil}
        self.ArticleInfo = ArticleInfo
    }
}
