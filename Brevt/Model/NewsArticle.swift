//
//  NewsArticle.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation
import UIKit

/**
 A Content object created from the JSON response from the content API
 
 - Parameters
 - withTitle: String that represents article title
 - andURL: String representing the article URL
 - withUniqueID: String that is the id from the database
 */
class NewsArticle {
    var title: String
    var url: String
    var articleID: String
    
    init() {
        self.title = ""
        self.url = ""
        self.articleID = "0"
    }
    
    init(withTitle title: String, andURL URL: String, withUniqueID ID: String) {
        self.title = title
        self.url = URL
        self.articleID = ID
        self.totalResults = 0
    }
    
    var totalResults: Int?
    var description: String?
    var images: UIImage?
    var urlToImage: String?
    var source: String?
    var author: String?
    var ratingScore: Double?
    
}

