//
//  Message.swift
//  Brevt
//
//  Created by Sam Jensen on 5/24/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

struct Message {
    
    let sender : String?
    let senderID: String?
    let messageBody : String
    let timeStamp: Any
    
    init?(messageDictionary: [String: Any]) {
        guard let messageBody = messageDictionary["MessageBody"] as? String,
            let timeStamp = messageDictionary["TimeStamp"]
            else {return nil}
        
        self.messageBody = messageBody
        self.sender = messageDictionary["Sender"] as? String
        self.timeStamp = timeStamp
        self.senderID = messageDictionary["SenderID"] as? String
        
    }
    
}
