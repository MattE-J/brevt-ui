//
//  FeedbackComment.swift
//  Brevt
//
//  Created by Matt Jackels on 3/26/18.
//  Copyright © 2018 brevt. All rights reserved.
//

import Foundation

class FeedbackComment {
    var content_item: String
    var comment: String
    
    init(forContentItem contentItemID: String) {
        self.content_item = contentItemID
        self.comment = ""
    }
    
    var createdByDisplayName: String?
    var feedbackID: String?
    var qualityScore: Double?
    var createdByUID: String?
    var createdOnDate: String?
    var modifiedOnDate: String?
    
}
